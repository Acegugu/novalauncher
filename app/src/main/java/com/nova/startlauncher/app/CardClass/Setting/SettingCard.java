package com.nova.startlauncher.app.CardClass.Setting;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;

/**
 * 설정 카드
 */
public class SettingCard extends Card implements View.OnClickListener {

    TextView ownerPhoneNum;
    ImageView wifiImage, flashImage, taskImage, settingImage;
    //AudioManager audioManager;
    TextView wifiText, flashText, taskText, wifiProgressText;
    public Camera cam;
    LinearLayout wifilayout,flashlayout,tasklayout,settinglayout;

    EasyTracker easyTracker;
    TaskCleaner taskCleaner;
    Animation taskAnimation;
    ProgressBar wifiProgressBar;
    int taskCleanCount = 0;

    private WifiManager wifimanager = (WifiManager)context.getSystemService(context.WIFI_SERVICE);

    public SettingCard(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);

        init();
        setListener();
    }

    public void init(){

        ownerPhoneNum = (TextView)cardView.findViewById(R.id.setting_card_owner_phonenum);
        wifilayout = (LinearLayout)cardView.findViewById(R.id.setting_card_wifilayout);
        flashlayout = (LinearLayout)cardView.findViewById(R.id.setting_card_flashlayout);
        tasklayout = (LinearLayout)cardView.findViewById(R.id.setting_card_tasklayout);
        settinglayout = (LinearLayout)cardView.findViewById(R.id.setting_card_settinglayout);

        wifiImage = (ImageView)cardView.findViewById(R.id.setting_card_wifi);
        flashImage = (ImageView)cardView.findViewById(R.id.setting_card_flash);
        taskImage = (ImageView)cardView.findViewById(R.id.setting_card_taskicon);
        settingImage = (ImageView)cardView.findViewById(R.id.setting_card_gotosetting);

        wifiText = (TextView)cardView.findViewById(R.id.setting_card_wifitext);
        flashText = (TextView)cardView.findViewById(R.id.setting_card_flashtext);
        taskText = (TextView)cardView.findViewById(R.id.setting_card_tasktext);

        easyTracker = EasyTracker.getInstance(context);

        ownerPhoneNum.setText(getMyPhoneNumber());
        taskCleaner = new TaskCleaner(context);
        wifiProgressBar = (ProgressBar)cardView.findViewById(R.id.setting_card_wifi_progressbar);
        wifiProgressText = (TextView)cardView.findViewById(R.id.setting_card_wifi_progresstext);

        IntentFilter intentfilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentfilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        context.registerReceiver(SettingCardReceiver, intentfilter);

        //TaskAnimation init
        taskAnimation = AnimationUtils.loadAnimation(context,R.anim.task_clean);
        taskAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                taskImage.setImageResource(R.drawable.broom_recolor);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                taskImage.setImageResource(R.drawable.broom_gray);
                Toast.makeText(context,taskCleanCount+"개의 앱을 청소 했습니다.",2000).show();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    @Override
    public void setListener(){

        wifilayout.setOnClickListener(this);
        flashlayout.setOnClickListener(this);
        tasklayout.setOnClickListener(this);
        settinglayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.setting_card_flashlayout :
                if (!BuildConfig.DEBUG) {
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Click Light", Long.valueOf(12)).build());
                }
                FlashlightAsyncTask flashlightAsyncTask = new FlashlightAsyncTask();
                flashlightAsyncTask.execute();
                break;
            case R.id.setting_card_settinglayout :
                if (!BuildConfig.DEBUG) {
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "More Setting", Long.valueOf(13)).build());
                }
                context.startActivity((new Intent(context, SettingActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)));
                break;
            case R.id.setting_card_wifilayout:
                wifimanager.setWifiEnabled(!wifimanager.isWifiEnabled());
                if(!wifimanager.isWifiEnabled())
                {
                    wifiProgressBar.setVisibility(View.VISIBLE);
                    wifiImage.setVisibility(View.INVISIBLE);
                    wifiText.setVisibility(View.INVISIBLE);
                    wifiProgressText.setVisibility(View.VISIBLE);
                    Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wifiProgressBar.setVisibility(View.INVISIBLE);
                            wifiImage.setVisibility(View.VISIBLE);
                            wifiText.setVisibility(View.VISIBLE);
                            wifiProgressText.setVisibility(View.INVISIBLE);
                        }
                    }, 2000);
                }
                if (!BuildConfig.DEBUG){
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Wifi OnOff", Long.valueOf(6)).build());
                }
                break;
            case R.id.setting_card_tasklayout:
                taskCleaner.countCleanableTasks();
                taskCleanCount = taskCleaner.cleanTasks();
                taskImage.startAnimation(taskAnimation);
                if (!BuildConfig.DEBUG){
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Task Clean", Long.valueOf(6)).build());
                }
                break;
        }

    }

    public void setWifiSwitchImage()
    {
        if(wifimanager.isWifiEnabled()) {
            wifiImage.setImageResource(R.drawable.wifion_recolor);
            wifiText.setText("WiFi On");
        }
        else {
            wifiImage.setImageResource(R.drawable.wifioff_recolor);
            wifiText.setText("WiFi Off");
        }
    }


    private String getMyPhoneNumber() {
        TelephonyManager systemService = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumber = systemService.getLine1Number();
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            phoneNumber = phoneNumber.substring(phoneNumber.length() - 10, phoneNumber.length());
            phoneNumber = "0" + phoneNumber;

            phoneNumber = PhoneNumberUtils.formatNumber(phoneNumber);   // 하이픈(-) 추가. 안먹힘
            return phoneNumber;
        }
        return "연락 가능한 기기가 아닙니다";
    }

    private BroadcastReceiver SettingCardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction() == WifiManager.NETWORK_STATE_CHANGED_ACTION) {
                NetworkInfo networkinfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                if (networkinfo != null) {
                    //setWifiSSID();
                }
            }
            else if (intent.getAction() == WifiManager.WIFI_STATE_CHANGED_ACTION) {
                setWifiSwitchImage();
            }
        }
    };

    private class FlashlightAsyncTask extends AsyncTask<Void, Integer, String> {

        @Override
        protected String doInBackground(Void... params) {
            if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                if (cam == null)
                    cam = Camera.open();

                Camera.Parameters p = cam.getParameters();
                String flash_mode = p.getFlashMode();
                if (flash_mode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    cam.setParameters(p);
                    cam.startPreview();
                } else {
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    cam.setParameters(p);
                    cam.stopPreview();
                }
                return flash_mode;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                if (result.equals(Camera.Parameters.FLASH_MODE_OFF)) {
                    flashImage.setImageResource(R.drawable.lighton_recolor);
                    flashText.setText("손전등 On");
                } else {
                    flashImage.setImageResource(R.drawable.lightoff_recolor);
                    flashText.setText("손전등 Off");
                }
            } else {
                Toast.makeText(context, "손전등을 지원하지 않는 스마트폰입니다", Toast.LENGTH_SHORT).show();
            }
        }
    } // End of FlashlightAsyncTask
}
