package com.nova.startlauncher.app.CardClass.AllApp;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;

/**
 * Created by FlaShilver on 2014. 5. 1..
 */
public class AllAppCard extends Card implements View.OnClickListener {

    LinearLayout enter_button;
    public static final int CALL_FROM_ALL_APP_CARD = 0;


    public AllAppCard(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);


        init();
        setListener();
    }

    private void init() {

        enter_button = (LinearLayout)cardView.findViewById(R.id.allapp_card_enter);
    }

    @Override
    public void setListener() {
        enter_button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (!BuildConfig.DEBUG){

            EasyTracker easyTracker = EasyTracker.getInstance(context);
            easyTracker.send(MapBuilder.createEvent("Press Card","Card Press","AllAppCard", Long.valueOf(6)).build());

        }


        Intent intent = new Intent(context, AllAppActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("callStage", CALL_FROM_ALL_APP_CARD);
        context.startActivity(intent);
    }
}
