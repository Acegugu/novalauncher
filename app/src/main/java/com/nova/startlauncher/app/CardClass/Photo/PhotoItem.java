package com.nova.startlauncher.app.CardClass.Photo;

import android.widget.ImageView;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by kmkyoung on 2014. 5. 29..
 */
public class PhotoItem {
    private String name;
    private String date;
    private String location;
    private ImageView imageview;

    PhotoItem(String name, String date, String location)
    {
        this.name = name;
        setDate(date);
        this.location = location;
    }

    public void setImageView(ImageView imageview)
    {
        this.imageview = imageview;
    }
    private void setDate(String date)
    {
        if(date.indexOf("-") == -1) {
            Calendar calender = Calendar.getInstance();
            calender.setTimeInMillis(Long.decode(date));
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            this.date = dateformat.format(calender.getTime());
        }
        else this.date = date;
    }

    public String getName() {return name;}
    public String getDate() {return date;}
    public String getLocation() {return location;}
    public ImageView getImageview() {return imageview;}


}
