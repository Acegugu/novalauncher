package com.nova.startlauncher.app.Util;

/**
 * Created by FlaShilver on 2014. 5. 4..
 */
public class FrequenteAppModel {

    String packageName;
    int useCount;

    public FrequenteAppModel() {
    }

    public FrequenteAppModel(String packageName) {
        this.packageName = packageName;
    }

    public FrequenteAppModel(String packageName, int useCount) {
        this.packageName = packageName;
        this.useCount = useCount;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }
}
