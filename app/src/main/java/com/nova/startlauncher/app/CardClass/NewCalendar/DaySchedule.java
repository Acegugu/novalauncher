package com.nova.startlauncher.app.CardClass.NewCalendar;

import java.util.Calendar;

/**
 * 일정 Model
 */
class DaySchedule {
    public long _id;
    public Calendar start;
    public String description;
    public int isAllday;

    DaySchedule(long _id, Calendar start, String description, int isAllday) {
        this._id = _id;
        this.start = start;
        this.description = description;
        this.isAllday = isAllday;
    }

    public DaySchedule(String description) {
        this.description = description;
    }
}
