package com.nova.startlauncher.app.Icon;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.nova.startlauncher.app.R;

/**
 * Created by FlaShilver on 2014. 5. 2..
 */
public class FavoriteIcon extends AppIcon {

    private Drawable appIcon;
    private String appTitle;


    public FavoriteIcon(Context context) {
        super(context);

        setAppIcon();
        setAppTitle();
    }

    public void Play(View v, Activity activity) {

    }

    public void setAppTitle(){
        this.appTitle = "즐겨찾기";
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon() {
        this.appIcon = context.getResources().getDrawable(R.drawable.bookmarkicon);
    }

    public String getAppTitle() {
        return appTitle;
    }
}
