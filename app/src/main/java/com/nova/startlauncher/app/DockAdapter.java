package com.nova.startlauncher.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.Favorite.FavoriteActivity;
import com.nova.startlauncher.app.Icon.FavoriteIcon;
import com.nova.startlauncher.app.Icon.AppIcon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by kmkyoung on 2014. 5. 2..
 */
public class DockAdapter extends BaseAdapter {
    LayoutInflater inflater;
    Context context;
    Activity activity;
    PackageManager packageManager;
    private ArrayList<AppIcon> apps = new ArrayList<AppIcon>();

    Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"));
    Intent smsIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:"));
    Intent internetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.android.com"));
    Intent cameraIntent = new Intent(new Intent(MediaStore.ACTION_IMAGE_CAPTURE));

    EasyTracker easyTracker;


    DockAdapter(Context context, Activity activity) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
        this.context = context;
        packageManager = context.getPackageManager();
        easyTracker = EasyTracker.getInstance(context);

        try {
            setDock();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setDock() throws PackageManager.NameNotFoundException {
        String dialerPackageName;
        String smsPackageName;
        String internetPackageName;
        String cameraPackageName;

        ResolveInfo dial = packageManager.resolveActivity(dialIntent, PackageManager.MATCH_DEFAULT_ONLY);
        dialerPackageName = dial.activityInfo.packageName;
        if (dialerPackageName.equals("android"))
            dialerPackageName = FistInstallApp(dialIntent);

        ResolveInfo sms = packageManager.resolveActivity(smsIntent, PackageManager.MATCH_DEFAULT_ONLY);
        smsPackageName = sms.activityInfo.packageName;
        if (smsPackageName.equals("android"))
            smsPackageName = FistInstallApp(smsIntent);

        ResolveInfo internet = packageManager.resolveActivity(internetIntent, PackageManager.MATCH_DEFAULT_ONLY);
        internetPackageName = internet.activityInfo.packageName;
        if (internetPackageName.equals("android"))
            internetPackageName = FistInstallApp(internetIntent);

        ResolveInfo camera = packageManager.resolveActivity(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
        cameraPackageName = camera.activityInfo.packageName;
        if (cameraPackageName.equals("android"))
            cameraPackageName = FistInstallApp(cameraIntent);

        AppIcon dialer = new AppIcon(dialerPackageName, context);
        dialer.setisCallIcon(true);

        apps.add(dialer);
        apps.add(new AppIcon(smsPackageName, context));
        apps.add(new FavoriteIcon(context));
        apps.add(new AppIcon(internetPackageName, context));
        apps.add(new AppIcon(cameraPackageName, context));

    }

    public String FistInstallApp(Intent intent) {
        Map map = new HashMap();
        List<ResolveInfo> homeApps = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (int i = 0; i < homeApps.size(); i++) {
            ResolveInfo info = homeApps.get(i);
            String App = info.activityInfo.packageName;
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(App, PackageManager.GET_PERMISSIONS);
                map.put(App, packageInfo.firstInstallTime);
            } catch (PackageManager.NameNotFoundException e) {
                map.put(App, Long.MAX_VALUE);
            }
        }
        Iterator<String> set = map.keySet().iterator();
        return set.next();
    }

    @Override
    public int getCount() {
        return apps.size();
    }


    @Override
    public Object getItem(int position) {
        return apps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.icon_item, parent, false);
        }


        ImageView iconImage = (ImageView) convertView.findViewById(R.id.dock_item_icon_image);
        TextView iconText = (TextView) convertView.findViewById(R.id.dock_item_icon_text);

        if (position == 2) {
            iconImage.setImageDrawable(context.getResources().getDrawable(R.drawable.bookmarkicon));
            iconText.setText("즐겨찾기");

            iconImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FavoriteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (!BuildConfig.DEBUG) {

                        easyTracker.send(MapBuilder.createEvent("Press Icon", "Icon Press", "PressFavorite", Long.valueOf(17)).build());

                    }
                    context.startActivity(intent);
                }
            });

        } else {

            iconImage.setImageDrawable(apps.get(position).getAppIcon());
            iconText.setText(apps.get(position).getAppTitle());

            iconImage.setOnClickListener(new ImageView.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position == 2) {
                        apps.get(position).Play(v, activity);
                    } else {
                        if (!BuildConfig.DEBUG) {

                            easyTracker.send(MapBuilder.createEvent("Press Icon", "Dock Icon Press", "Press" + apps.get(position).getAppTitle(), Long.valueOf(18)).build());

                        }
                        apps.get(position).Play();
                    }
                }
            });

        }
        iconText.setTextColor(Color.WHITE);

        return convertView;
    }
}