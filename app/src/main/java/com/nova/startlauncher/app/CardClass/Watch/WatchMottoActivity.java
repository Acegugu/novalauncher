package com.nova.startlauncher.app.CardClass.Watch;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nova.startlauncher.app.R;

/**
 * Created by kmkyoung on 2014. 5. 22..
 */
public class WatchMottoActivity extends Activity implements View.OnClickListener{
    private static final String defaultFont = "SeoulNamsanB.otf";
    private TextView textView1,textView2;
    private EditText mottoEditText;
    private Button okButton;
    private Button cancelButton;
    private LinearLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watchmotto);

        setLayout();
        setListener();
        setBackground();

    }

    private void setBackground()
    {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Setting", getApplicationContext().MODE_MULTI_PROCESS);
        String background = sharedpreferences.getString("SettingBackground", "");

        if(Build.VERSION.SDK_INT >= 16)
        {
            if(background.equals(""))
                relativeLayout.setBackground(getResources().getDrawable(R.drawable.background1));
            else
                relativeLayout.setBackground(getResources().getDrawable(getResources().getIdentifier(background,null,getPackageName())));
        }
        else
        {
            if(background.equals(""))
                relativeLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1));
            else
                relativeLayout.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background,null,getPackageName())));
        }

        relativeLayout.setAlpha(0.7f);
    }

    private void setFont()
    {
        textView1.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), defaultFont));
        textView2.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), defaultFont));
        okButton.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), defaultFont));
        cancelButton.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), defaultFont));
        mottoEditText.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), defaultFont));
    }

    private void setLayout()
    {
        textView1 = (TextView)findViewById(R.id.textView1);
        textView2 = (TextView)findViewById(R.id.textView2);
        okButton = (Button)findViewById(R.id.watch_motto_ok);
        cancelButton = (Button)findViewById(R.id.watch_motto_cancel);
        mottoEditText = (EditText)findViewById(R.id.watch_motto_edit);
        relativeLayout = (LinearLayout)findViewById(R.id.watch_motto_layout);
    }

    private void setListener()
    {
        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager inputMethodManager = (InputMethodManager) getApplicationContext().getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(mottoEditText.getWindowToken(), 0);
                return false;
            }
        });
    }

    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.watch_motto_ok:
                Intent intent = new Intent();
                intent.putExtra("watch_motto",mottoEditText.getText().toString());
                setResult(RESULT_OK,intent);
                break;

            case R.id.watch_motto_cancel:
                setResult(RESULT_CANCELED);
                break;

            default:
                break;
        }
        finish();
    }

}
