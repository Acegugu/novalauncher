package com.nova.startlauncher.app.CardClass.NewCalendar;

import com.nova.startlauncher.app.R;

import java.util.Calendar;

/**
 * 날씨 Model
 */
class Weather {
    Calendar date;
    String state;
    int low;
    int high;
    int skycodeday;

    Weather(Calendar date, String state, int low, int high, int skycodeday) {
        this.date = date;
        this.state = state;
        this.low = low;
        this.high = high;
        this.skycodeday = skycodeday;
    }

    public int getWeatherImageId() {
        int id = 0;
        if (this.skycodeday == 4 || this.skycodeday == 9 || this.skycodeday == 11) //  4 == T자형 폭풍, 소나기
            id = R.drawable.weather_rain;
        else if (this.skycodeday == 28 || this.skycodeday == 30 || this.skycodeday == 34) // 대체로 흐림, 갬, 약간 흐림
            id = R.drawable.weather_cloudy;
        else if (this.skycodeday == 32) // 구름 없음
            id = R.drawable.weather_sunny;
        else if (this.skycodeday == 39) // 소나기 후 맑음
            id = R.drawable.weather_sunny_after_rain;
        return id;
    }
}
