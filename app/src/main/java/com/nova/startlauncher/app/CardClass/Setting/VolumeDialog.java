package com.nova.startlauncher.app.CardClass.Setting;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.SeekBar;
import android.media.AudioManager;

import com.nova.startlauncher.app.R;

/**
 * Created by FlaShilver on 2014. 4. 6..
 */
public class VolumeDialog extends Dialog implements SeekBar.OnSeekBarChangeListener {

    private SeekBar mediaSeekbar;
    private SeekBar ringtoneSeekbar;
    private SeekBar alarmSeekbar;

    int mediaVolumeVal;
    int ringthoneVolumeVal;
    int alarmVolumeVal;
    private AudioManager audioManager = null;

    public VolumeDialog(Context context) {
        super(context);

        init();
    }

    public void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_setting_card_volume);

        audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        mediaVolumeVal = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        ringthoneVolumeVal = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        alarmVolumeVal = audioManager.getStreamVolume(AudioManager.STREAM_ALARM);


        mediaSeekbar = (SeekBar) findViewById(R.id.setting_card_media_seekbar);
        mediaSeekbar.setOnSeekBarChangeListener(this);
        mediaSeekbar.setProgress(mediaVolumeVal);
        mediaSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        ringtoneSeekbar = (SeekBar) findViewById(R.id.setting_card_ringtone_seekbar);
        ringtoneSeekbar.setOnSeekBarChangeListener(this);
        ringtoneSeekbar.setProgress(ringthoneVolumeVal);
        ringtoneSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

        alarmSeekbar = (SeekBar) findViewById(R.id.setting_card_alarm_seekbar);
        alarmSeekbar.setOnSeekBarChangeListener(this);
        alarmSeekbar.setProgress(alarmVolumeVal);
        alarmSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if(seekBar.getId()==R.id.setting_card_media_seekbar){
            mediaVolumeVal = progress;

            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mediaVolumeVal, 0);

        }else if (seekBar.getId()==R.id.setting_card_ringtone_seekbar){
            ringthoneVolumeVal = progress;

            audioManager.setStreamVolume(AudioManager.STREAM_RING, ringthoneVolumeVal, 0);

        }else if (seekBar.getId()==R.id.setting_card_alarm_seekbar){
            alarmVolumeVal = progress;

            audioManager.setStreamVolume(AudioManager.STREAM_ALARM, alarmVolumeVal, 0);

        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
