package com.nova.startlauncher.app.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FlaShilver on 2014. 5. 4..
 */
public class FrequentDBHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "FrequenteManager";

    // Contacts table name
    private static final String TABLE_RECOMMAND = "FrequenteAppCount";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PACKAGENAME = "packagename";
    private static final String KEY_COUNT = "count";

    public FrequentDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_FAVORITE_APP_TABLE = "create table " + TABLE_RECOMMAND + "("
                + KEY_ID + " integer primary key autoincrement," + KEY_PACKAGENAME + " text not null,"
                + KEY_COUNT + " integer);";
        db.execSQL(CREATE_FAVORITE_APP_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists" + TABLE_RECOMMAND);

        onCreate(db);

    }

    public void addApp(String packageName) {
        if (!packageName.equals("com.nova.startlauncher.app"))
        {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();

            String rawQuery = "select * from " + TABLE_RECOMMAND + " where packagename='" + packageName + "';";

            if (0 == db.rawQuery(rawQuery, null).getCount()) {
                FrequenteAppModel model = new FrequenteAppModel(packageName, 0);

                values.put(KEY_PACKAGENAME, model.getPackageName());
                values.put(KEY_COUNT, model.getUseCount());
                db.insert(TABLE_RECOMMAND, null, values);

                db.close();
            } else {
                String getCountQuery = "select count from " + TABLE_RECOMMAND + " where packagename='" + packageName + "';";

                Cursor cursor = db.rawQuery(getCountQuery, null);
                try {
                    cursor.moveToFirst();

                    int preCount = Integer.parseInt(cursor.getString(0));
                    updateCount(preCount, packageName);
                }finally {
                    cursor.close();
                }

                db.close();
            }
        }
    }

    public List<FrequenteAppModel> getAppsThreeList() {
        List<FrequenteAppModel> modelList = new ArrayList<FrequenteAppModel>();

        String selectQuery = "select * from FrequenteAppCount order by count desc limit 3;";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    FrequenteAppModel model = new FrequenteAppModel();
                    model.setPackageName(cursor.getString(1));
                    model.setUseCount(Integer.parseInt(cursor.getString(2)));
                    modelList.add(model);
                } while (cursor.moveToNext());
            }
        } finally {
            cursor.close();
        }

        return modelList;
    }

    public void updateCount(int preCount, String packageName) {

        SQLiteDatabase db = this.getWritableDatabase();
        preCount++;

        ContentValues values = new ContentValues();
        values.put(KEY_COUNT, preCount);
        db.update(TABLE_RECOMMAND, values, KEY_PACKAGENAME + "=?", new String[]{packageName});

        db.close();
    }

}
