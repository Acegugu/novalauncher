package com.nova.startlauncher.app.Util;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.os.SystemClock;

import java.util.List;

public class AppCountService extends Service {

    private final String LOGTAG = "COUNTER_";
    private boolean TREADSTATE = true;
    private ComponentName RECENTCALLED;
    private FrequentDBHelper db;
    private ApplicationInfo app;

    ActivityManager mActivityManager;

    public AppCountService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        db = new FrequentDBHelper(getApplicationContext(), "FrequenteManager", null, 1);

        mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);


        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (TREADSTATE) {
                    List<ActivityManager.RecentTaskInfo> info = mActivityManager.getRecentTasks(1, Intent.FLAG_ACTIVITY_NEW_TASK);

                    if (info != null) {

                        ActivityManager.RecentTaskInfo recent = info.get(0);
                        Intent intent = recent.baseIntent;
                        ComponentName componentName = intent.getComponent();
                        //Log.w(LOGTAG, "pref App ComponentName : " + RECENTCALLED);
                        //Log.w(LOGTAG, "recent App ComponentName : " + componentName);

                        try {
                            app = getPackageManager().getApplicationInfo(componentName.getPackageName(), 0);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }

                        if (intent.getComponent().equals(RECENTCALLED)) {
                            //Log.w(LOGTAG, "pref App, recent App Same App");
                        } else if (getPackageManager().getLaunchIntentForPackage(app.packageName) != null) { //패치키명을 통하여 앱이런칭 가능한 앱만 넣습니다. // 시스템앱은 제외.
                            RECENTCALLED = componentName;

                            db.addApp(componentName.getPackageName());

                            //Log.e(LOGTAG, "Application is catched. App ComponentName is " + componentName.toString());
                        }
                        db.getAppsThreeList();
                    }
                    SystemClock.sleep(5000);
                }
            }
        });

        thread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }
}
