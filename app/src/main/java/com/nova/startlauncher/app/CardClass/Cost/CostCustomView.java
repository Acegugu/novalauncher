package com.nova.startlauncher.app.CardClass.Cost;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.Utility;

import java.util.Arrays;
import java.util.List;


/**
 * Created by kmkyoung on 2014. 5. 30..
 */
public class CostCustomView extends View {
    private final int MAX_COLOR = 7;
    private int maxCount=0, currentCount=0;
    private long currentDataCount=0;
    private long maxDataCount=0;
    private int state = 0; //0 = call, 1 = sms, 2 = data
    private int width, height;
    private final float innerRadius = (float)1/5;
    private final float middleRadius = (float)4/18;
    private final float outterRadius = (float)5/18;
    private List<Integer> colorList = Arrays.asList(R.color.cost_circle1,R.color.cost_circle2,R.color.cost_circle3,R.color.cost_circle4,R.color.cost_circle5,R.color.cost_circle6,R.color.cost_circle7);

    public CostCustomView(Context context) {
        super(context);
    }

    public CostCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CostCustomView(Context context, AttributeSet attrs, int defstyle) {
        super(context, attrs, defstyle);
    }

    public void setMaxCount(int count)
    {
        maxCount = count;
    }

    public void setMaxCount(long count)
    {
        maxDataCount = count;
    }

    public void setState(int state)
    {   //1 is data, 0 is call,sms
        this.state = state;
    }

    public void setCurrentCount(int count)
    {
        currentCount = count;
    }

    public void setCurrentCount(long count)
    {
        currentDataCount = count;
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec)
    {
        width = MeasureSpec.getSize(widthSpec);
        height = MeasureSpec.getSize(heightSpec);

        setMeasuredDimension(width,height);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        Paint paint = new Paint();
        paint.setAntiAlias(true);

        paint.setColor(getResources().getColor(R.color.cost_defult_gray));
        RectF rf = new RectF(width/2-width*outterRadius, height/2-width*outterRadius, width/2+width*outterRadius, height/2+width*outterRadius);

        canvas.drawCircle(width/2, height/2, width*outterRadius, paint);


        if(state == 2)
        {
            if(currentDataCount > 0 ) {
                double calcu = currentDataCount / (maxDataCount * MAX_COLOR);
                if (calcu == 0)
                    paint.setColor(getResources().getColor(colorList.get((int) (currentDataCount / maxDataCount))));
                else
                    paint.setColor(getResources().getColor(colorList.get((int) (currentDataCount % (maxDataCount * MAX_COLOR) / maxDataCount))));

                canvas.drawArc(rf, -90, 360 * ((float)(currentDataCount%maxDataCount) / maxDataCount), true, paint);
            }
        }
        else
        {
            if(currentCount != 0) {
                int calcu = currentCount / (maxCount * MAX_COLOR);
                if (calcu == 0)
                    paint.setColor(getResources().getColor(colorList.get(currentCount / maxCount)));
                else
                    paint.setColor(getResources().getColor(colorList.get(currentCount % (maxCount * MAX_COLOR) / maxCount)));

                canvas.drawArc(rf, -90, 360 * (float) (currentCount%maxCount) / maxCount, true, paint);
            }
        }

        paint.setColor(Color.WHITE);
        canvas.drawCircle(width/2, height/2, width*middleRadius, paint);
        paint.setColor(getResources().getColor(R.color.cost_center_darkgray));
        canvas.drawCircle(width/2, height/2, width*innerRadius, paint);

        Bitmap icon = null;
        switch(state)
        {
            case 0:
                icon = BitmapFactory.decodeResource(getResources(), R.drawable.charge_call);
                break;
            case 1:
                icon = BitmapFactory.decodeResource(getResources(),R.drawable.charge_message);
                break;
            case 2:
                icon = BitmapFactory.decodeResource(getResources(),R.drawable.charge_internet);
                break;
        }
        if(icon!=null)
            canvas.drawBitmap(icon,(width/2-width*innerRadius)+3,height/2-width*innerRadius,paint);

    }


}
