package com.nova.startlauncher.app.CardClass.NewContactBook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.ContactHelper;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;

/**
 * 새로운 연락처 카드
 * 공격력: ★★★★☆☆
 */
public class NewContactBook extends Card implements View.OnClickListener {


    private static final String[] koreanOrder = {"가", "나", "다", "라", "마", "바", "사", "아", "자", "차", "카", "타", "파", "하"};
    private static final int SEND_CONTACT_CHANGED = 1;
    private static final String contactMenuFont = "SeoulNamsanB.otf";
    private int PAGES = 0;
    private int MENU = 0;
    private String INDEX = "";

    private ViewPager viewPager;
    private TableLayout contactMenu;

    private ContactHelper contactHelper;
    private ContactPagerAdapter contactPagerAdapter;
    private ArrayList<Contact> contacts;
    private ArrayList<Contact> source;
    private EasyTracker easyTracker;
    private ContactChangedHandler contactChangedHandler;
    public ViewPagerMoveBroadcastReceiver viewPagerMoveBroadcastReceiver;
    public static String VIEWPAGER_ACTION = "com.nova.startlauncher.app.CardClass.NewContactBook.ViewPagerMove";

    public NewContactBook(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);
        init();
        setListener();
    }

    private void init() {
        easyTracker = EasyTracker.getInstance(context);

        contactHelper = new ContactHelper(context.getContentResolver());
        contacts = contactHelper.getContacts();

        viewPager = (ViewPager) cardView.findViewById(R.id.contact_viewpager);
        contactMenu = (TableLayout) cardView.findViewById(R.id.contact_menu);

        contactPagerAdapter = new ContactPagerAdapter(((FragmentActivity)context).getSupportFragmentManager());
        viewPager.setAdapter(contactPagerAdapter);

        CirclePageIndicator circlePageIndicator = (CirclePageIndicator) cardView.findViewById(R.id.contact_viewpager_indicator);
        circlePageIndicator.setViewPager(viewPager);
        circlePageIndicator.setFillColor(context.getResources().getColor(R.color.contact_indicator_fill));
        circlePageIndicator.setStrokeColor(context.getResources().getColor(R.color.contact_indicator_stroke));
        circlePageIndicator.setPageColor(context.getResources().getColor(R.color.contact_indicator_page));

        for (int i = 0; i < contactMenu.getChildCount(); i++)
        {
            TableRow row = (TableRow) contactMenu.getChildAt(i);
            for (int j = 0; j < row.getChildCount(); j++){
                Button menu = (Button) row.getChildAt(j);
                if (menu != null) {
                    menu.setTypeface(Typeface.createFromAsset(context.getAssets(), contactMenuFont));
                    menu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            viewPager.setAdapter(null);
                            switchMenu((Integer.valueOf(v.getTag().toString())));
                            viewPager.setAdapter(contactPagerAdapter);
                        }
                    });
                }
            }
        }

        ContactObserver contactObserver = new ContactObserver();
        context.getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, contactObserver);

        contactChangedHandler = new ContactChangedHandler();
        viewPagerMoveBroadcastReceiver = new ViewPagerMoveBroadcastReceiver();
        context.registerReceiver(viewPagerMoveBroadcastReceiver, new IntentFilter(VIEWPAGER_ACTION));
    }

    private void switchMenu(int menu) {
        if (MENU == menu * 2 || MENU == 15 || MENU == 17) {
            MENU = (menu-1)*2 + 1;
        } else if (MENU == (menu-1)*2 + 1) {
            MENU += 1;
        } else {
            MENU = (menu-1)*2 + 1;
        }
        changeSource();
    }

    private void changeSource(int menu) {
        MENU = menu;
        changeSource();
    }

    private void changeSource() {
        source = null;
        String trackingMenu = "ContactBook Menu";

        if (MENU == 0) {
            source = contacts;
            INDEX = "전체 보기";
        }
        else if (MENU <= koreanOrder.length) {
            // 가나다 순
            source = new ArrayList<Contact>();
            for (Contact item : contacts) {
                String lastName = String.valueOf(item.name.charAt(0));
                if (MENU == koreanOrder.length && lastName.compareToIgnoreCase(koreanOrder[MENU-1]) >= 0)
                    source.add(item);
                else if (lastName.compareToIgnoreCase(koreanOrder[MENU-1]) >= 0 && lastName.compareToIgnoreCase(koreanOrder[MENU]) < 0)
                    source.add(item);
            }
            INDEX = koreanOrder[MENU-1];
            trackingMenu = "ContactBook Menu Korean";
        } else {
            if(MENU == 15) { // 자주 찾는 번호
                source = contactHelper.getFrequentlyContacts(4);
                INDEX = "자주 찾는 번호";
                trackingMenu = "ContactBook Menu Frequently";
            }
            else { // 전체 보기
                source = contacts;
                INDEX = "전체 보기";
                trackingMenu = "ContactBook Menu All";
            }
        }
        if (!BuildConfig.DEBUG) {

            easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", trackingMenu, Long.valueOf(8)).build());

        }
        PAGES = source.size() != 0 && source.size() % 4 == 0 ? source.size() / 4 : source.size() / 4 + 1;
    }

    private ArrayList<Contact> getContactsWithPageIndex(int index) {
        ArrayList<Contact> pageContacts = new ArrayList<Contact>();
        for (int i = 0; i < 4; i++)
        {
            int itemIndex = index*4+i;
            if(source.size() > itemIndex)
                pageContacts.add(source.get(itemIndex));
        }

        return pageContacts;
    }

    @Override
    public void setListener() {

    }

    @Override
    public void onClick(View v) {

    }

    /**
     * 연락처 변경 시, 알림
     */
    private class ContactObserver extends ContentObserver {

        public ContactObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            contactChangedHandler.sendEmptyMessage(SEND_CONTACT_CHANGED);
        }
    }

    /**
     * 연락처 변경 시, 데이터 새로 가져옴
     */
    private class ContactChangedHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case SEND_CONTACT_CHANGED:
                    viewPager.setAdapter(null);
                    contacts = contactHelper.getContacts();
                    changeSource();
                    viewPager.setAdapter(contactPagerAdapter);
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * 연락처 ViewPager Adapter
     */
    private class ContactPagerAdapter extends FragmentStatePagerAdapter {

        public ContactPagerAdapter(FragmentManager fm) {
            super(fm);
            source = contactHelper.getFrequentlyContacts(4);
            PAGES = source.size() % 4 == 0 ? source.size() / 4 : source.size() / 4 + 1;
            MENU = 15;
            INDEX = "자주 찾는 번호";
        }

        @Override
        public int getCount() {
            return PAGES;
        }

        @Override
        public Fragment getItem(int position) {
            if (!BuildConfig.DEBUG) {

                easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "ContactBook Swipe Name List", Long.valueOf(8)).build());

            }
            return ContactFragment.newInstance(INDEX, getContactsWithPageIndex(position));
        }
    }

    /**
     * ViewPager Move Broadcast Receiver
     */
    public class ViewPagerMoveBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String state = bundle.getString("DATA");
            if (state.equals("PREVIOUS")) {
                int previous_index = viewPager.getCurrentItem() - 1;
                if (previous_index == -1) {
                    int menu_index = MENU - 1 == 0 ? 1 : MENU - 1;
                    if (menu_index != MENU) {   // '가' 항목 첫번째에서 다시 '가' 항목 마지막으로 가는 것을 막기 위함
                        refreshViewPager(menu_index);
                        viewPager.setCurrentItem(PAGES - 1);
                    }
                } else {
                    viewPager.setCurrentItem(previous_index, true);
                }
            } else if (state.equals("NEXT")) {
                int next_index = viewPager.getCurrentItem() + 1;
                if (next_index + 1 > PAGES) {
                    int menu_index = MENU + 1 == 16 ? 15 : MENU + 1;
                    refreshViewPager(menu_index);
                } else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                }
            }
        }

        private void refreshViewPager(int menu_index) {
            viewPager.setAdapter(null);
            changeSource(menu_index);
            viewPager.setAdapter(contactPagerAdapter);
        }
    }
}
