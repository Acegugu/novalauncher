package com.nova.startlauncher.app.Favorite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FlaShilver on 2014. 5. 7..
 */
public class FavoriteDBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "FavoriteManager";

    // Contacts table name
    private static final String TABLE_FAVORITE = "FavoriteAppTable";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PACKAGENAME = "packagename";

    public FavoriteDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_FAVORITE_APP_TABLE = "create table " + TABLE_FAVORITE + "("
                + KEY_ID + " integer primary key autoincrement," + KEY_PACKAGENAME + " text not null);";

        db.execSQL(CREATE_FAVORITE_APP_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("drop table if exists" + TABLE_FAVORITE);

        onCreate(db);

    }

    public void setFavoriteAppsList(List<FavoriteAppModel> list) {
        for (FavoriteAppModel item : getFavoriteAppsList())
            deleteApp(item.getPackageName());

        for (FavoriteAppModel item : list)
            addApp(item.getPackageName());
    }

    public boolean addApp(String packageName) {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        String rawQuery = "select * from " + TABLE_FAVORITE + " where packagename='" + packageName + "';";

        if (0 == db.rawQuery(rawQuery, null).getCount()) {
            FavoriteAppModel model = new FavoriteAppModel(packageName);

            values.put(KEY_PACKAGENAME, model.getPackageName());
            db.insert(TABLE_FAVORITE, null, values);

            db.close();

            return true;
        } else {
            db.close();

            return false;
        }
    }

    public List<FavoriteAppModel> getFavoriteAppsList() {
        List<FavoriteAppModel> modelList = new ArrayList<FavoriteAppModel>();

        String selectQuery = "select * from " + TABLE_FAVORITE + ";";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        try{
            if (cursor.moveToFirst()) {
                do {
                    FavoriteAppModel model = new FavoriteAppModel();

                    model.setPackageName(cursor.getString(1));
                    modelList.add(model);
                } while (cursor.moveToNext());
            }
        }finally {
            cursor.close();
        }

        return modelList;
    }

    public void deleteApp(String packageName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FAVORITE, KEY_PACKAGENAME + "=?", new String[]{packageName});
        db.close();
    }

    public long getSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + TABLE_FAVORITE + " where 1;";
        Cursor cursor = db.rawQuery(query, null);
        long itemSize = cursor.getCount();
        try{
            itemSize = cursor.getCount();
        }finally {
            cursor.close();
        }
        db.close();
        return itemSize;
    }
}
