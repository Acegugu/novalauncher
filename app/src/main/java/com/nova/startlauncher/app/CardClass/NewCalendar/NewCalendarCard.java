package com.nova.startlauncher.app.CardClass.NewCalendar;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.ibm.icu.util.ChineseCalendar;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.CardLauncherActivity;
import com.nova.startlauncher.app.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import fr.castorflex.android.flipimageview.library.FlipImageView;

public class NewCalendarCard extends Card implements View.OnClickListener {

    private static final String titleFont = "SeoulNamsanB.otf";
    //View
    GridView dayGridView;
    DayGridViewAdapter dayGridViewAdapter;
    TextView solarDateTextView;

    Button moreButton = null;
    Button schedule_addButton = null;
    //Model
    ArrayList<NewCalendarDayModel> newCalendarDayModels = null;
    private ViewGroup mContainerView = null;
    FlipImageView imageView;

    EasyTracker easyTracker;
    private int preFlipPosition;


    public NewCalendarCard(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);

        init();
        setListener();
        setWeekInfo();
        setView();
    }

    private void init() {
        dayGridView = (GridView) cardView.findViewById(R.id.newcalendar_card_daylist);
        dayGridViewAdapter = new DayGridViewAdapter();

        solarDateTextView = (TextView) cardView.findViewById(R.id.newcalendar_card_solar_date);
        solarDateTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), titleFont));

        mContainerView = (ViewGroup) cardView.findViewById(R.id.new_day_container);

        moreButton = (Button) cardView.findViewById(R.id.newcalendar_more_button);
        moreButton.setTypeface(Typeface.createFromAsset(context.getAssets(), titleFont));

        schedule_addButton = (Button) cardView.findViewById(R.id.newcalendar_schedule_add_button);
        schedule_addButton.setTypeface(Typeface.createFromAsset(context.getAssets(), titleFont));

        easyTracker = EasyTracker.getInstance(context);

        newCalendarDayModels = new ArrayList<NewCalendarDayModel>();

        IntentFilter calendar_intentFilter = new IntentFilter("android.intent.action.PROVIDER_CHANGED");
        calendar_intentFilter.addDataScheme("content");
        calendar_intentFilter.addDataAuthority("com.android.calendar", null);

        IntentFilter date_changed_intentFilter = new IntentFilter();
        date_changed_intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        date_changed_intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);

        //쓰레드를 시작해야함.
    }

    public void makeView(int position) {
        //makeView는 오직 한번만 호출되는중임.
        mContainerView.removeAllViews();

        Calendar dtstart = Calendar.getInstance();
        dtstart.add(Calendar.DAY_OF_MONTH, position);

        Calendar dtend = (Calendar) dtstart.clone();
        dtend.add(Calendar.DAY_OF_MONTH, 1);

        // 파라미터 기간에 맞는 일정을 뷰에 가져옴

        getScheduleWithDateRange(dtstart, dtend);
    }

    // 파라미터 기간의 각 날짜에 대하여 일정 목록으로 뷰를 만드는 addDaySchedule Method를 호출
    private void getScheduleWithDateRange(Calendar dtstart, Calendar dtend) {
        Cursor cur;
        ContentResolver cr = context.getContentResolver();
        Uri uri = CalendarContract.Events.CONTENT_URI;

        // 하루 단위로 일정을 가져와서 일정 목록을 만들고 그것으로 뷰를 만듬
        Calendar dateCursor = (Calendar) dtstart.clone();
        while (dateCursor.before(dtend)) {
            Calendar todayStart = (Calendar) dateCursor.clone();
            Calendar todayEnd = (Calendar) dateCursor.clone();

            todayStart.set(Calendar.HOUR_OF_DAY, 0);
            todayStart.set(Calendar.MINUTE, 0);
            todayStart.set(Calendar.SECOND, 0);
            todayEnd.set(Calendar.HOUR_OF_DAY, 24);
            todayEnd.set(Calendar.MINUTE, 0);
            todayEnd.set(Calendar.SECOND, 0);

            Uri.Builder builder = CalendarContract.Instances.CONTENT_URI.buildUpon();
            ContentUris.appendId(builder, todayStart.getTimeInMillis());
            ContentUris.appendId(builder, todayEnd.getTimeInMillis());

            // query 실행
            cur = cr.query(builder.build(), new String[]{"event_id"}, null, null, CalendarContract.Instances.DTSTART + " ASC");

            ArrayList<DaySchedule> schedule_list = null;
            if (cur != null) {
                schedule_list = new ArrayList<DaySchedule>();

                // 매 일정마다 DaySchedule model instance 생성하여 list에 저장
                DaySchedule daySchedule;
                while (cur.moveToNext()) {
                    long id = cur.getLong(0);

                    String selection = CalendarContract.Events._ID + " == ?";
                    String[] selectionArgs = new String[]{String.valueOf(id)};
                    Cursor event_cursor = cr.query(uri, new String[]{"_id", CalendarContract.Events.TITLE, CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND, CalendarContract.Events.ALL_DAY}, selection, selectionArgs, CalendarContract.Events.DTSTART + " ASC");

                    if (event_cursor != null) {
                        event_cursor.moveToFirst();

                        String description = event_cursor.getString(1);

                        Calendar start = Calendar.getInstance();
                        Calendar end = Calendar.getInstance();

                        start.setTimeInMillis(event_cursor.getLong(3));
                        end.setTimeInMillis(event_cursor.getLong(4));

                        int isAllday = event_cursor.getInt(5);

                        daySchedule = new DaySchedule(id, start, description, isAllday);

                        if (start.get(Calendar.DAY_OF_MONTH) == todayStart.get(Calendar.DAY_OF_MONTH) && schedule_list.size() < 3) {
                            schedule_list.add(daySchedule);
                        }

                        event_cursor.close();
                    }
                } // end of inner while
            }
            if (schedule_list.size() == 0)   // 일정이 없는 날에 대해서는 "일정 없음"을 표기하기 위한 fake model 생성
                schedule_list.add(new DaySchedule("일정 없음"));

            // 날짜와 일정 목록으로 view 생성
            addDaySchedule(dateCursor, schedule_list);
            dateCursor.add(Calendar.DAY_OF_MONTH, 1);   // 다음 날로 커서 이동

            cur.close();
        } // end of outer while
    }

    // 날짜와, 하루치의 일정 목록으로 뷰를 만듦
    public void addDaySchedule(Calendar date, ArrayList<DaySchedule> schedule_list) {
        final ViewGroup newView = (ViewGroup) LayoutInflater.from(context).inflate(
                R.layout.calendar_item, mContainerView, false);

        newView.setTag(date.clone());

        LinearLayout calendar_schedule_list = (LinearLayout) newView.findViewById(R.id.calendar_schedule_list);

        // 하루의 일정 목록을 LinearLayout에 add
        for (DaySchedule item : schedule_list) {
            calendar_schedule_list.addView(getDayScheduleView(item, calendar_schedule_list.getChildCount()));
        }

        // LinearLayout에 추가
        mContainerView.addView(newView);
    }

    public View getDayScheduleView(final DaySchedule daySchedule, int position) {
        ViewHolder holder;
        final DateFormat df = new SimpleDateFormat("a hh:mm"); // 12시 표시법

        LayoutInflater inflater = LayoutInflater.from(context);
        View convertView = inflater.inflate(R.layout.schedule_item, null);

        holder = new ViewHolder();

        holder.time = (TextView) convertView.findViewById(R.id.time);
        holder.time.setTypeface(Typeface.createFromAsset(context.getAssets(), titleFont));

        holder.description = (TextView) convertView.findViewById(R.id.description);
        holder.description.setTypeface(Typeface.createFromAsset(context.getAssets(), titleFont));



        if (daySchedule.start != null) {
            if (daySchedule.isAllday == 1)
                holder.time.setText("종일");
            else
                holder.time.setText(df.format(daySchedule.start.getTime()));
        }
        holder.description.setText(daySchedule.description);

        // 각 일정 클릭 시, 해당 일정의 캘랜더 상세 화면으로 이동
        // TODO(YM) - 일정 없음에 대해서 이벤트 적용하지 않도록
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("events");
                ContentUris.appendId(builder, daySchedule._id);

                Intent intent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "DetailSchedule", Long.valueOf(8)).build());

                }
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    class ViewHolder {
        public TextView time;
        public TextView description;
    }

    @Override
    public void setListener() {
        moreButton.setOnClickListener(this);
        schedule_addButton.setOnClickListener(this);
    }

    public void setView() {
        dayGridView.setAdapter(dayGridViewAdapter);

        Calendar today = Calendar.getInstance();
        ChineseCalendar chineseCalendar = CardLauncherActivity.chineseCalendar;
        chineseCalendar.setTimeInMillis(today.getTimeInMillis());

        solarDateTextView.setText(newCalendarDayModels.get(0).getTodaySolorDate());
        makeView(0);
    }

    public void setDateText(int position) {
        solarDateTextView.setText(newCalendarDayModels.get(position).getTodaySolorDate());
    }

    public void setWeekInfo() {
        for (int i = 0; i < 7; i++) {
            Calendar today = Calendar.getInstance();
            today.add(Calendar.DATE, i);
            newCalendarDayModels.add(new NewCalendarDayModel(today.get(Calendar.DAY_OF_WEEK), today, context));
        }
    }

    public void resetDayImage(int position) {
        for (int i = 0; i < 7; i++) {
            if (position == i) {

            } else {
                View view = dayGridView.getChildAt(i);
                FlipImageView flipImageView = (FlipImageView) view.findViewById(R.id.newcalendar_card_day_unit_images);
                if (Build.VERSION.SDK_INT >= 16) {
                    flipImageView.setBackground(context.getResources().getDrawable(R.drawable.sche_back_deselect));
                } else {
                    flipImageView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.sche_back_deselect));
                }
                flipImageView.setFlipped(false, true);
            }
        }
    }

    @Override
    public void onClick(View v) {

        Uri.Builder builder;
        Intent intent;

        switch (v.getId()) {
            case R.id.newcalendar_more_button:
                // 더보기 버튼
                builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, System.currentTimeMillis());

                intent = new Intent(Intent.ACTION_VIEW).setData(builder.build());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                context.startActivity(intent);


                break;
            case R.id.newcalendar_schedule_add_button:
                // 일정 등록 버튼
                // TODO 갤럭시 2에서 action에 해당하는 activity가 없다
                intent = new Intent(Intent.ACTION_INSERT).setData(CalendarContract.Events.CONTENT_URI);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(context, "달력 보기에서 추가해주세요!", Toast.LENGTH_LONG).show();
                }
                break;

            default:
                break;
        }
    }

    private class DayGridViewAdapter extends BaseAdapter {

        LayoutInflater inflater;

        private DayGridViewAdapter() {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return newCalendarDayModels.size();
        }

        @Override
        public NewCalendarDayModel getItem(int position) {
            return newCalendarDayModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            dayGridViewAdapter.notifyDataSetChanged();

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.newcalendarcard_day_unit, parent, false);
            }

            imageView = (FlipImageView) convertView.findViewById(R.id.newcalendar_card_day_unit_images);
            imageView.setImageDrawable(context.getResources().getDrawable(getItem(position).getDayDrawable()));
            imageView.setFlippedDrawable(context.getResources().getDrawable(newCalendarDayModels.get(position).getWeatherResource()));
            imageView.setOnFlipListener(new FlipImageView.OnFlipListener() {
                @Override
                public void onClick(FlipImageView view) {
                    if (Build.VERSION.SDK_INT >= 16) {
                        view.setBackground(context.getResources().getDrawable(R.drawable.sche_back_select));
                    } else {
                        view.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.sche_back_select));
                    }
                    setDateText(position);
                    makeView(position); //클릭하면 해당 날짜에 맞는 View를 만들어주게하려고 넣음.
                    resetDayImage(position);
                }

                @Override
                public void onFlipStart(FlipImageView view) {
                }

                @Override
                public void onFlipEnd(FlipImageView view) {
                }
            });

            return convertView;
        }
    }
}