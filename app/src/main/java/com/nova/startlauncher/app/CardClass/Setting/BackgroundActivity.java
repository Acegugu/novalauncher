package com.nova.startlauncher.app.CardClass.Setting;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.nova.startlauncher.app.CardLauncherActivity;
import com.nova.startlauncher.app.R;

/**
 * Created by kmkyoung on 2014. 5. 23..
 */
public class BackgroundActivity extends Activity implements View.OnClickListener{
    private ViewFlipper backgroundViewFlipper;
    private Button background_pre;
    private Button background_next;
    private Button background_select;
    private final int MAX_IMAGE_SIZE = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_background);

        setLayout();
        setListener();

        for(int i=1; i<=MAX_IMAGE_SIZE ;i++)
        {
            View v = getLayoutInflater().inflate(R.layout.background_layout,null);
            ImageView imageview = (ImageView)v.findViewById(R.id.setting_background_example);
            imageview.setImageDrawable(getResources().getDrawable(getResources().getIdentifier("drawable/background"+i,null,getPackageName())));
            backgroundViewFlipper.addView(v);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.setting_background_pre:
                backgroundViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_pre_in));
                backgroundViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_pre_out));
                backgroundViewFlipper.showPrevious();
                break;
            case R.id.setting_background_next:
                backgroundViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_next_in));
                backgroundViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide_next_out));
                backgroundViewFlipper.showNext();
                break;
            case R.id.setting_background_select:
                selectBackGround();
                break;
        }

    }

    private void selectBackGround()
    {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Setting",getApplicationContext().MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        String backgroundName = "drawable/background"+(backgroundViewFlipper.getDisplayedChild()+1);
        editor.putString("SettingBackground", backgroundName);
        editor.commit();
        CardLauncherActivity.backgroundChange = true;

        finish();

    }

    private void setLayout(){
        backgroundViewFlipper = (ViewFlipper) findViewById(R.id.setting_background_viewFliper);
        background_select = (Button)findViewById(R.id.setting_background_select);
        background_pre = (Button) findViewById(R.id.setting_background_pre);
        background_next = (Button) findViewById(R.id.setting_background_next);
    }

    public void setListener()
    {
        background_select.setOnClickListener(this);
        background_pre.setOnClickListener(this);
        background_next.setOnClickListener(this);
    }

}
