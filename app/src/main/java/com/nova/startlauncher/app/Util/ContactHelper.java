package com.nova.startlauncher.app.Util;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds;

import com.nova.startlauncher.app.CardClass.NewContactBook.Contact;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * ContactsContract Helper
 * Now Using for Contact Photo Thumbnail,
 * to get Contact with contact id,
 * to get PhoneNumber with contact id
 */
public final class ContactHelper {
    private ContentResolver contentResolver;

    private static final String[] PHOTO_ID_PROJECTION = new String[] {
            Contacts.PHOTO_ID
    };

    private static final String[] PHOTO_BITMAP_PROJECTION = new String[] {
            CommonDataKinds.Photo.PHOTO
    };

    public ContactHelper(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    /**
     * 연락처의 photo id로 thumbnail을 가져옴
     * @param thumbnailId 가져올 thumbnail의 photo id
     * @return 이미지 - Thumbnail Bitmap
     */
    public final Bitmap fetchThumbnail(final int thumbnailId) {
        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        final Cursor cursor = contentResolver.query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                }
            }
            return thumbnail;
        }
        finally {
            cursor.close();
        }
    }

    /**
     * contact id에 해당하는 raw image bitmap
     * @param id contact id
     * @return Contact Bitmap
     */
    public final Bitmap loadContactRawPhoto(long id)
    {
        Uri uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, id);
        InputStream input = ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
        if (input == null) {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }

    /**
     * Contact object list를 반환함
     * @return Contact object list
     */
    public final ArrayList<Contact> getContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        String selection = Contacts.HAS_PHONE_NUMBER + " == ?";
        String[] selectionArgs = new String[]{"1"};

        final Uri uri = Contacts.CONTENT_URI;
        Cursor contact_cur = contentResolver
                .query(uri, null, selection, selectionArgs, Contacts.DISPLAY_NAME + " ASC");

        while (contact_cur.moveToNext()) {
            String id = contact_cur.getString(contact_cur.getColumnIndex(Contacts._ID));
            String name = contact_cur.getString(contact_cur.getColumnIndex(Contacts.DISPLAY_NAME));
            String phone_number = getPhoneNumberWithContactId(id);
            int photo_id = contact_cur.getInt(contact_cur.getColumnIndex(Contacts.PHOTO_ID));

            Contact item = new Contact(id, name, phone_number, photo_id);
            contacts.add(item);
        }
        contact_cur.close();
        return contacts;
    }

    /**
     * contact id로 Contact object를 반환함
     * @param contact_id 가져올 Contact object의 id
     * @return Contact object
     */
    public final Contact getContactWithContactId(int contact_id) {
        final Uri uri = ContentUris.withAppendedId(Contacts.CONTENT_URI, contact_id);
        Cursor contact_cur = contentResolver
                .query(uri, null, null, null, null);

        if (contact_cur.moveToFirst()) {
            String id = contact_cur.getString(contact_cur.getColumnIndex(Contacts._ID));
            String name = contact_cur.getString(contact_cur.getColumnIndex(Contacts.DISPLAY_NAME));
            String phone_number = getPhoneNumberWithContactId(id);
            int photo_id = contact_cur.getInt(contact_cur.getColumnIndex(Contacts.PHOTO_ID));

            Contact result = new Contact(id, name, phone_number, photo_id);

            contact_cur.close();
            return result;
        }
        return null;
    }

    /**
     * contact id로 전화번호를 얻음
     * @param contact_id 가져올 전화번호에 해당하는 연락처의 id
     * @return 전화번호
     */
    public final String getPhoneNumberWithContactId(String contact_id){
        String selection = CommonDataKinds.Phone.CONTACT_ID + " == ?";
        String[] selectionArgs = new String[]{contact_id};
        Cursor phone_cur = contentResolver.query(CommonDataKinds.Phone.CONTENT_URI, new String[]{CommonDataKinds.Phone.NUMBER}, selection, selectionArgs, null);

        String phone_number = null;
        if (phone_cur.moveToNext())
            phone_number = phone_cur.getString(phone_cur.getColumnIndex(CommonDataKinds.Phone.NUMBER));

        phone_cur.close();
        return phone_number;
    }

    /**
     *  자주 거는(전화 횟수 기준) 연락처를 파라미터의 수만큼 가져옴
     *  @param limit 가져올 자주 거는 연락처 수(최빈순)
     *  @return Contact object array frequently called
     **/
    public final ArrayList<Contact> getFrequentlyContacts(int limit) {
        String selection = Contacts.HAS_PHONE_NUMBER + " == ?";
        String[] selectionArgs = new String[]{"1"};

        Cursor cur = contentResolver.query(Contacts.CONTENT_URI, new String[]{Contacts._ID, Contacts.DISPLAY_NAME, Contacts.PHOTO_ID, Contacts.TIMES_CONTACTED, Contacts.HAS_PHONE_NUMBER}, selection, selectionArgs, Contacts.TIMES_CONTACTED + " DESC LIMIT " + limit);

        ArrayList<Contact> contact_list = new ArrayList<Contact>();

        while (cur.moveToNext()) {
            String id = cur.getString(cur.getColumnIndex(Contacts._ID));
            String name = cur.getString(cur.getColumnIndex(Contacts.DISPLAY_NAME));
            String phone_number = getPhoneNumberWithContactId(id);
            int photo_id = cur.getInt(cur.getColumnIndex(Contacts.PHOTO_ID));

            Contact contact = new Contact(id, name, phone_number, photo_id);
            contact_list.add(contact);
        }
        cur.close();

        return contact_list;
    }

    /**
     * Starred Contact object list를 반환함
     * @return Starred contact object list
     */
    public final ArrayList<Contact> getStarredContacts() {
        ArrayList<Contact> contacts = new ArrayList<Contact>();

        String selection = Contacts.HAS_PHONE_NUMBER + " == ? AND " + Contacts.STARRED + " == ?";
        String[] selectionArgs = new String[]{"1", "1"};

        final Uri uri = Contacts.CONTENT_URI;
        Cursor contact_cur = contentResolver
                .query(uri, null, selection, selectionArgs, Contacts.DISPLAY_NAME + " ASC");

        while (contact_cur.moveToNext()) {
            String id = contact_cur.getString(contact_cur.getColumnIndex(Contacts._ID));
            String name = contact_cur.getString(contact_cur.getColumnIndex(Contacts.DISPLAY_NAME));
            String phone_number = getPhoneNumberWithContactId(id);
            int photo_id = contact_cur.getInt(contact_cur.getColumnIndex(Contacts.PHOTO_ID));

            Contact item = new Contact(id, name, phone_number, photo_id);
            contacts.add(item);
        }
        contact_cur.close();
        return contacts;
    }
}
