package com.nova.startlauncher.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.ibm.icu.util.ChineseCalendar;
import com.nova.startlauncher.app.CardClass.AllApp.AllAppCard;
import com.nova.startlauncher.app.CardClass.Cost.CostCard;
import com.nova.startlauncher.app.CardClass.NewCalendar.NewCalendarCard;
import com.nova.startlauncher.app.CardClass.NewContactBook.NewContactBook;
import com.nova.startlauncher.app.CardClass.Photo.PhotoCard;
import com.nova.startlauncher.app.CardClass.Search.SearchCard;
import com.nova.startlauncher.app.CardClass.Setting.SettingCard;
import com.nova.startlauncher.app.CardClass.Watch.WatchCard;
import com.nova.startlauncher.app.Guide.GuideActivity;
import com.nova.startlauncher.app.Util.AppCountService;

import java.util.Map;

public class CardLauncherActivity extends FragmentActivity implements AdapterView.OnItemSelectedListener {

    public static ChineseCalendar chineseCalendar = new ChineseCalendar();

    private static final String GA_PROPERTY_ID = "UA-50706998-1";
    private static final String SCREEN_LABEL = "Home Screen";
    private static final String CAMPAIGN_SOURCE_PARAM = "utm_source";
    public final static String FIRSTRUN = "FIRSTRUN";
    public static Boolean backgroundChange = true;

    private LinearLayout listView;
    private ScrollView cardScrollView;
    private int cardCount = -1;

    private GridView dockGridView;
    private Context context;

    private WatchCard watchCard;
    private NewContactBook newContactBook;
    private PhotoCard photoCard;
    private SearchCard searchCard;
    private CostCard costCard;
    private SettingCard settingCard;
    private AllAppCard allAppCard;
    private NewCalendarCard newCalendarCard;
    private SharedPreferences prefs;

    Tracker mTracker;
    private int scrollX;
    private int scrollY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTracker = GoogleAnalytics.getInstance(this).getTracker(GA_PROPERTY_ID);

        listView = (LinearLayout) findViewById(R.id.mCardList);
        cardScrollView = (ScrollView) findViewById(R.id.cardScrollView);

        context = getApplicationContext();

        watchCard = new WatchCard(R.layout.watch_card, "Watch&Photo Card", getApplicationContext(), ++cardCount);
        photoCard = new PhotoCard(R.layout.photo_card,"Photo Card",getApplicationContext(),++cardCount);
        newContactBook = new NewContactBook(R.layout.new_contact_book, "New Contact Book Card", this, ++cardCount);
        searchCard = new SearchCard(R.layout.search_card, "Search Card", getApplicationContext(), ++cardCount);
        costCard = new CostCard(R.layout.cost_card, "Cost Card", getApplicationContext(), ++cardCount);
        settingCard = new SettingCard(R.layout.setting_card, "Setting Card", getApplicationContext(), ++cardCount);
        allAppCard = new AllAppCard(R.layout.allapp_card, "AllApp Card", getApplicationContext(), ++cardCount);
        newCalendarCard = new NewCalendarCard(R.layout.newcalendar_card,"NewCalendar Card", getApplicationContext(), ++cardCount);

        listView.addView(watchCard.getCardView());
        listView.addView(photoCard.getCardView());
        listView.addView(newContactBook.getCardView());
        listView.addView(searchCard.getCardView());
        listView.addView(costCard.getCardView());
        listView.addView(newCalendarCard.getCardView());
        listView.addView(settingCard.getCardView());
        listView.addView(allAppCard.getCardView());

        watchCard.setActivity(this);

        cardScrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        // 검색 카드 - 키보드 숨김
                        searchCard.hideKeyboard();
                        break;
                }

                return false;
            }
        });

        dockGridView = (GridView) findViewById(R.id.activity_main_dock);
        dockGridView.setAdapter(new DockAdapter(context, this));

        startService(new Intent(this, AppCountService.class));

        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!this.isFinishing()){
            scrollX = cardScrollView.getScrollX();
            scrollY = cardScrollView.getScrollY();
            searchCard.cleanEditText();
            Camera camera = settingCard.cam;
            if (camera != null) {
                camera.release();
                settingCard.cam = null;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean(FIRSTRUN, true)) {
            startActivity(new Intent(getApplicationContext(), GuideActivity.class));
        }
        //this is important. scrollTo doesn't work in main thread.
        cardScrollView.post(new Runnable()
        {
            @Override
            public void run()
            {
                cardScrollView.scrollTo(scrollX, scrollY);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!BuildConfig.DEBUG){

            EasyTracker.getInstance(this).activityStart(this);

            mTracker.set(Fields.SCREEN_NAME, SCREEN_LABEL);

            Intent intent = this.getIntent();
            Uri uri = intent.getData();

            MapBuilder.createAppView().setAll(getReferrerMapFromUri(uri));

        }

        BackgroundChange();
        photoCard.photoUpdate();
    }

    private void BackgroundChange()
    {
        if(backgroundChange)
        {
            RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.mainlayout);
            SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Setting", getApplicationContext().MODE_MULTI_PROCESS);
            String background = sharedpreferences.getString("SettingBackground", "");
            if(Build.VERSION.SDK_INT >= 16)
            {
                if(background.equals(""))
                    relativeLayout.setBackground(getResources().getDrawable(R.drawable.background1));
                else
                    relativeLayout.setBackground(getResources().getDrawable(getResources().getIdentifier(background,null,getPackageName())));
            }
            else
            {
                if(background.equals(""))
                    relativeLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1));
                else
                    relativeLayout.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background,null,getPackageName())));
            }
            backgroundChange = false;
        }
    }

    private Map<String, String> getReferrerMapFromUri(Uri uri) {
        MapBuilder paramMap = new MapBuilder();

        // If no URI, return an empty Map.
        if (uri == null) { return paramMap.build(); }

        // Source is the only required campaign field. No need to continue if not
        // present.
        if (uri.getQueryParameter(CAMPAIGN_SOURCE_PARAM) != null) {

            // MapBuilder.setCampaignParamsFromUrl parses Google Analytics campaign
            // ("UTM") parameters from a string URL into a Map that can be set on
            // the Tracker.
            paramMap.setCampaignParamsFromUrl(uri.toString());

            // If no source parameter, set authority to source and medium to
            // "referral".
        } else if (uri.getAuthority() != null) {
            paramMap.set(Fields.CAMPAIGN_MEDIUM, "referral");
            paramMap.set(Fields.CAMPAIGN_SOURCE, uri.getAuthority());

        }

        return paramMap.build();    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!BuildConfig.DEBUG){
            EasyTracker.getInstance(this).activityStop(this);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        costCard.onDestroy();
        photoCard.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        watchCard.onActivityResult(requestCode,resultCode,data);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 1) {
            //Log.d("keyboard", "item 1 selected");
            // listView.setItemsCanFocus(true);

            // Use afterDescendants, because I don't want the ListView to steal focus
            listView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
            searchCard.searchEditRequestFocus();
        } else {
            //Log.d("keyboard", "scroll others");
            if (!listView.isFocused()) {
                // listView.setItemsCanFocus(false);

                // Use beforeDescendants so that the EditText doesn't re-take focus
                listView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
                listView.requestFocus();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // This happens when you start scrolling, so we need to prevent it from staying
        // in the afterDescendants mode if the EditText was focused
        //Log.d("keyboard", "nothing selected");
        listView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
    }
}