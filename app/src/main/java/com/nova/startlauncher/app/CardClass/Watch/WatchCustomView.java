package com.nova.startlauncher.app.CardClass.Watch;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.nova.startlauncher.app.R;

import java.io.IOException;


/**
 * Created by kmkyoung on 2014. 5. 4..
 */
public class WatchCustomView extends View {
    Bitmap imageBitmap = null;
    String fileName = null;
    float rotation = -40;
    Camera camera = new Camera();
    Matrix matrix = new Matrix();
    Boolean setSize = false;
    Boolean isImagePath = false;
    int viewWidth;
    int viewHeight;

    public WatchCustomView(Context context) {
        super(context);
        init();
    }

    public WatchCustomView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public WatchCustomView(Context context, AttributeSet attributeSet, int defStyleAttr) {
        super(context, attributeSet, defStyleAttr);
    }

    public void init() {
        SharedPreferences sharedpreferences = getContext().getSharedPreferences("WatchPhoto", Context.MODE_MULTI_PROCESS);
        String filePath = sharedpreferences.getString("ImagePath", "");
        if (filePath == "") {
            imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.defult_gallery);
        }
        else {
            fileName = filePath;
            isImagePath = true;
        }
        camera.setLocation((float) 0, (float) 1.5, (float) -8.0);
        camera.rotateY(rotation);
        camera.getMatrix(matrix);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        if (!setSize && !isImagePath) {
            viewWidth = getWidth();
            viewHeight = getHeight();
            setSize = true;
            if(imageBitmap == null)
                imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.defult_gallery);
            canvas.drawBitmap(imageBitmap, 0, 0, null);
        }
        else {
            //canvas.drawBitmap(imageBitmap,matrix,null);
            if (imageBitmap == null) {

                viewWidth = getWidth();
                viewHeight = getHeight();

                try {
                    setBitmapImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            canvas.drawBitmap(imageBitmap, 0, 0, null);
        }
        super.onDraw(canvas);
    }



    public void setBitmapImage() throws IOException {
        Log.d("kmky", "setBitmapImage");
        int orientation_val = 0;
        int srcWidth = 0;
        int srcHeight = 0;

        ExifInterface exifInterface = new ExifInterface(fileName);
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,1);
        if(orientation == 6)
            orientation_val = 90;
        else if(orientation == 3)
            orientation_val = 180;
        else if(orientation == 8)
            orientation_val =270;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileName, options);

        if(orientation_val ==90 || orientation_val ==270)
        {
            srcWidth = options.outHeight;
            srcHeight = options.outWidth;
        }
        else
        {
            srcWidth = options.outWidth;
            srcHeight = options.outHeight;
        }

        if(srcWidth > viewWidth || srcHeight > viewHeight)
        {
            float widthScale = srcWidth / viewWidth;
            float heightScale = srcHeight / viewHeight;
            float scale = Math.max(widthScale,heightScale);
            options.inJustDecodeBounds = false;
            options.inSampleSize = (int)scale;
            imageBitmap = BitmapFactory.decodeFile(fileName,options);
        }
        else
            imageBitmap = BitmapFactory.decodeFile(fileName);

        if(orientation_val > 0)
        {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation_val);
            imageBitmap = Bitmap.createBitmap(imageBitmap,0,0,imageBitmap.getWidth(),imageBitmap.getHeight(),matrix,true);
        }

        srcWidth = imageBitmap.getWidth();
        srcHeight = imageBitmap.getHeight();

        if (srcWidth != getWidth() || srcHeight != getHeight()) {
            float widthScale = (float)srcWidth / (float)viewWidth;
            float heightScale = (float)srcHeight / (float)viewHeight;
            float scale = Math.max(widthScale, heightScale);
            srcWidth = (int)((float)srcWidth / scale);
            srcHeight = (int)((float)srcHeight / scale);
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, srcWidth, srcHeight, true);
        }
    }

    public void setFileName(String string)
    {
        fileName = string;
        SharedPreferences sharedpreferences = getContext().getSharedPreferences("WatchPhoto", Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ImagePath", fileName);
        editor.commit();
        try {
            setBitmapImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        invalidate();
    }
}
