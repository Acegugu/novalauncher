package com.nova.startlauncher.app.Guide;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.nova.startlauncher.app.CardLauncherActivity;
import com.nova.startlauncher.app.Favorite.FavoriteDBHelper;
import com.nova.startlauncher.app.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 앱 가이드 Activity
 */
public class GuideActivity extends FragmentActivity {
    static final int NUM_ITEMTS = 6;

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private Button nextButton;
    private FavoriteDBHelper favoritedb;

    public final static Integer[] imageResIds = new Integer[]{
            R.drawable.guide_content_0, R.drawable.guide_content_1,
            R.drawable.guide_content_2, R.drawable.guide_content_3,
            R.drawable.guide_content_4, R.drawable.guide_content_0};

    public final static Integer[] subjectStringResIds = new Integer[]{
            R.string.fragment_page_subject_0, R.string.fragment_page_subject_1,
            R.string.fragment_page_subject_2, R.string.fragment_page_subject_3,
            R.string.fragment_page_subject_4, R.string.fragment_page_subject_5};

    public final static Integer[] detailStringResIds = new Integer[]{
            R.string.fragment_page_detail_0, R.string.fragment_page_detail_1,
            R.string.fragment_page_detail_2, R.string.fragment_page_detail_3,
            R.string.fragment_page_detail_4, R.string.fragment_page_detail_5};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        mViewPager = (ViewPager) findViewById(R.id.activity_guide_pager);
        mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        setFavoriteAppSet();

        //Bind the title indicator to the adapter
        CirclePageIndicator circleIndicator = (CirclePageIndicator) findViewById(R.id.activity_guide_indicator);
        circleIndicator.setViewPager(mViewPager);
        circleIndicator.setFillColor(getResources().getColor(R.color.guide_indicator_fill));
        circleIndicator.setStrokeColor(getResources().getColor(R.color.guide_indicator_stroke));
        circleIndicator.setPageColor(getResources().getColor(R.color.guide_indicator_page));
        circleIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //Log.d("pager", "selected "+position);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (position == NUM_ITEMTS - 1) {
                    final String txtStart = getString(R.string.guide_start_button);
                    nextButton.setText(txtStart);
                    if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        nextButton.setBackground(getResources().getDrawable(R.drawable.yellow_button));
                    else
                        nextButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.yellow_button));
                } else {
                    final String txtNext = getString(R.string.guide_next_button);
                    nextButton.setText(txtNext);
                    if (sdk >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        nextButton.setBackground(getResources().getDrawable(R.drawable.green_button));
                    else
                        nextButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.green_button));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Log.d("pager", "state changed "+state);
            }
        });

        nextButton = (Button) findViewById(R.id.fragment_page_next);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() != NUM_ITEMTS - 1)
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                else {
                    getSharedPreferences(getPackageName(), MODE_PRIVATE).edit().putBoolean(CardLauncherActivity.FIRSTRUN, false).commit();
                    finish();
                }
            }
        });
    }

    private void setFavoriteAppSet() {

        PackageManager pm = getPackageManager();
        favoritedb = new FavoriteDBHelper(getApplicationContext(), "FavoriteManager", null, 1);
        List<PackageInfo> packs = pm.getInstalledPackages(0);

        //갤러리시작
        Intent imageViewIntent = new Intent(Intent.ACTION_PICK);
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();
        Uri pictureDirectory = Uri.parse(path);
        imageViewIntent.setDataAndType(pictureDirectory, "image/jpg");
        List<ResolveInfo> galleryApps = getPackageManager().queryIntentActivities(imageViewIntent, PackageManager.MATCH_DEFAULT_ONLY);
        favoritedb.addApp(galleryApps.get(0).activityInfo.packageName);
        //갤러리끝

        //계산기 시작
        boolean findAcalcul = false;

        for (PackageInfo pi : packs) {
            if (findAcalcul == false) {
                if (pi.packageName.toString().toLowerCase().contains("calcul")) {
                    favoritedb.addApp(pi.packageName);
                    findAcalcul = true;
                }
            }
        }
        //계산기 끝

        //알람 앱 시작
        Intent alarmIntent = new Intent(AlarmClock.ACTION_SET_ALARM);
        List<ResolveInfo> alarmApps = getPackageManager().queryIntentActivities(alarmIntent, PackageManager.MATCH_DEFAULT_ONLY);
        favoritedb.addApp(alarmApps.get(0).activityInfo.packageName);
        //알람 앱 시작

        //PlayStore
        favoritedb.addApp("com.android.vending");

        //설정 앱 시작
        Intent settingIntent = new Intent(Settings.ACTION_SETTINGS);
        List<ResolveInfo> settingApps = getPackageManager().queryIntentActivities(settingIntent, PackageManager.MATCH_DEFAULT_ONLY);
        favoritedb.addApp(settingApps.get(0).activityInfo.packageName);
        //설정 앱 끝

        //연락처 앱 시작
        Intent contactIntent = new Intent(Intent.ACTION_INSERT);
        contactIntent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        List<ResolveInfo> contactApps = getPackageManager().queryIntentActivities(contactIntent, PackageManager.MATCH_DEFAULT_ONLY);
        favoritedb.addApp(contactApps.get(0).activityInfo.packageName);
        //연락처 앱 끝

        //메모 앱 시작
        boolean findAmemo = false;
        for (PackageInfo pi : packs) {
            if (findAmemo == false) {
                if (pi.packageName.toString().toLowerCase().contains("memo") || pi.packageName.toString().toLowerCase().contains("note")) {
                    favoritedb.addApp(pi.packageName);
                    findAmemo = true;
                }
            }
        }
        //메모 앱 끝

        //추천앱 상태 넣기
        SharedPreferences pref = getSharedPreferences("RecommandApps", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        if (appInstalledOrNot("com.kakao.talk")){
            editor.putInt("Kakaotalk", 1);
        }else{
            editor.putInt("Kakaotalk", 0);
        }

        if (appInstalledOrNot("com.whitecrow.metroid")){
            editor.putInt("Subway", 1);
        }else{
            editor.putInt("Subway", 0);
        }

        if (appInstalledOrNot("com.nova.medicinealram.app")){
            editor.putInt("drugalarm", 1);
        }else{
            editor.putInt("drugalarm", 0);
        }

        editor.commit();
    }
    @Override
    public void finish() {
        super.finish();
        this.overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    public boolean appInstalledOrNot(String uri){
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }catch (PackageManager.NameNotFoundException e){
            app_installed = false;
        }
        return app_installed;
    }

    /**
     * 앱 가이드 ViewPager Adapter
     */
    private class PagerAdapter extends FragmentStatePagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMTS;
        }

        @Override
        public Fragment getItem(int position) {
            return PageFragment.newInstance(position);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
