package com.nova.startlauncher.app.CardClass.Watch;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nova.startlauncher.app.R;

/**
 * Created by kmkyoung on 2014. 5. 5..
 */
public class WatchDialog extends Dialog implements View.OnClickListener{
    Context context;
    private Button WatchPhotoDialogOkButton, WatchPhotoDialogCancelButton;
    String motto;
    TextView MottoTextView;

    public WatchDialog(Context context, TextView MottoTextView) {
        super(context);
        setContentView(R.layout.watch_dialog);
        this.context = context;
        WatchPhotoDialogOkButton = (Button)findViewById(R.id.watchphoto_dialog_ok);
        WatchPhotoDialogCancelButton = (Button)findViewById(R.id.watchphoto_dialog_cancel);

        WatchPhotoDialogOkButton.setOnClickListener(this);
        WatchPhotoDialogCancelButton.setOnClickListener(this);
        this.MottoTextView = MottoTextView;

        setTitle("좌우명 입력");

    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.watchphoto_dialog_ok:
                getDialogInput();
                dismiss();
                break;
            case R.id.watchphoto_dialog_cancel:
                dismiss();
                break;
        }

    }

    public void getDialogInput()
    {

        EditText watchPhotoMottoEdit = (EditText)getWindow().findViewById(R.id.watchphoto_motto_edit);

        if(watchPhotoMottoEdit.getText().toString().length() == 0)
        {
            MottoTextView.setText(R.string.watch_motto_default);
            SharedPreferences sharedpreferences = context.getSharedPreferences("WatchPhoto",context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString("WatchPhotoMotto","");
            editor.commit();
        }
        else
        {
            SharedPreferences sharedpreferences = context.getSharedPreferences("WatchPhoto",context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            motto = watchPhotoMottoEdit.getText().toString();
            editor.putString("WatchPhotoMotto",motto);
            editor.commit();
            MottoTextView.setText(motto);
        }
    }
}
