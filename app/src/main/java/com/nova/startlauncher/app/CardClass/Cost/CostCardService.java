package com.nova.startlauncher.app.CardClass.Cost;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by kmkyoung on 2014. 4. 7..
 */
public class CostCardService extends Service {
    private final String TAG = "CostCardService";

    private CostCardCallObserver costcardCallobserver;
    private CostCardSmsObserver costCardSmsObserver;

    Messenger costCardMessenger;
    final Messenger serviceMessenger = new Messenger( new IncomingHandler());
    TrafficStats trafficstatus = new TrafficStats();

    Timer dataTimer = new Timer();
    TimerTask dataTimerTask = new TimerTask() {
        @Override
        public void run() {
            Long traffic = trafficstatus.getMobileRxBytes()+trafficstatus.getMobileTxBytes();
            SharedPreferences sharedpreferences = getSharedPreferences("Cost", Context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedpreferences.edit();

            Long initdata = sharedpreferences.getLong("CostInitData",0);
            Long predata = sharedpreferences.getLong("CostPreData",0);
            editor.putLong("CostUsingData", traffic-initdata+predata);
            editor.commit();
            try{
                if(costCardMessenger != null)
                    costCardMessenger.send(Message.obtain(null, CostCard.SEND_DATA_UPDATE));
            }
            catch(RemoteException e) { }

        }
    };

    class IncomingHandler extends Handler{
        @Override
        public void handleMessage( Message msg ){
            switch( msg.what ){
                case CostCard.REGISTER_CLIENT:
                    costCardMessenger = msg.replyTo;
                    break;

                default:
                    try{
                        costCardMessenger.send(msg);
                    }catch(Exception e){ }
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        costCardSmsObserver = new CostCardSmsObserver(new Handler(),getApplicationContext(), serviceMessenger);
        costcardCallobserver = new CostCardCallObserver(new Handler(),getApplicationContext(), serviceMessenger);
        getContentResolver().registerContentObserver(Uri.parse("content://sms"), true, costCardSmsObserver);
        getContentResolver().registerContentObserver(android.provider.CallLog.Calls.CONTENT_URI,true, costcardCallobserver);

        dataTimer.schedule(dataTimerTask, 5000, 300000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(costCardSmsObserver);
        getContentResolver().unregisterContentObserver(costcardCallobserver);
        dataTimer.cancel();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return serviceMessenger.getBinder();
    }
}
