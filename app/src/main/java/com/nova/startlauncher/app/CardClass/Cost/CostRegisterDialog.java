package com.nova.startlauncher.app.CardClass.Cost;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.nova.startlauncher.app.R;

/**
 * Created by kmkyoung on 2014. 4. 7..
 */
public class CostRegisterDialog extends Dialog implements View.OnClickListener {

    private View costRegisterView;
    private Context context;
    private Button costRegisterOkButton, costRegisterCancelButton;


    public CostRegisterDialog(Context context)
    {
        super(context);

        this.context = context;

        setTitle("알림 받을 사용량 수치를 입력해주세요");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.cost_register);

        costRegisterOkButton = (Button)findViewById(R.id.cost_register_ok_button);
        costRegisterCancelButton = (Button)findViewById(R.id.cost_register_cancel_button);
        costRegisterCancelButton.setOnClickListener(this);
        costRegisterOkButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.cost_register_ok_button:
                getDialogInput();
                dismiss();
                break;
            case R.id.cost_register_cancel_button:
                dismiss();
                break;

        }

    }

    public void getDialogInput()
    {

        EditText costWarningCall = (EditText)getWindow().findViewById(R.id.cost_register_warning_call);
        EditText costWarningSms = (EditText)getWindow().findViewById(R.id.cost_register_warning_sms);
        EditText costWarningData = (EditText)getWindow().findViewById(R.id.cost_register_warning_data);

        if(costWarningCall.getText().toString().length() == 0|| costWarningSms.getText().toString().length() == 0 || costWarningData.getText().toString().length() == 0)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("빈 칸이 있습니다");
            builder.setPositiveButton("확인",new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int id)
                {
                    show();
                }
            });
            builder.setNegativeButton("등록 취소",new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int id)
                {
                    dismiss();
                }
            });
            AlertDialog alertdialog = builder.create();
            alertdialog.setTitle("경고");
            alertdialog.show();
        }
        else
        {
            SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putLong("CostWarningCall",Long.valueOf(costWarningCall.getText().toString())*60);
            editor.putInt("CostWarningSms",Integer.valueOf(costWarningSms.getText().toString()));
            editor.putLong("CostWarningData",Long.valueOf(costWarningData.getText().toString())*1024*1024);
            editor.commit();
        }
    }
}
