package com.nova.startlauncher.app.CardClass.Cost;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

/**
 * Created by kmkyoung on 2014. 4. 11..
 */
public class CostCardSmsObserver extends ContentObserver {
    private final String TAG = "CostCardSmsObserver";
    private Context context;
    private final Uri SMS_STATUS_URI = Uri.parse("content://sms");
    private Messenger sendMessenger;

    public CostCardSmsObserver(Handler handler, Context context, Messenger messenger)
    {
        super(handler);
        this.context = context;
        sendMessenger = messenger;
    }

    @Override
    public void onChange(boolean selfChange)
    {
        super.onChange(selfChange);
        Cursor smsSentCursor = context.getContentResolver().query(SMS_STATUS_URI, null, null, null, null);
        if (smsSentCursor != null) {
            if (smsSentCursor.moveToFirst()) {
                //type 2 = send complete, 1 = receive
                if(smsSentCursor.getInt(smsSentCursor.getColumnIndex("type"))==2)
                {
                    SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    int sms = sharedpreferences.getInt("CostUsingSms", 0);
                    editor.putInt("CostUsingSms", ++sms);
                    editor.commit();

                    try{
                        if(sendMessenger != null)
                            sendMessenger.send(Message.obtain(null, CostCard.SEND_SMS_UPDATE));
                    }
                    catch(RemoteException e) { }
                }
            }
        }
    }
}
