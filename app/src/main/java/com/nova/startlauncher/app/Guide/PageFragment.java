package com.nova.startlauncher.app.Guide;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.Utility;

/**
 * 앱 가이드 ViewPager Fragment
 */
public class PageFragment extends Fragment {
    private static final String PAGE_NUM = "page";
    private int mPageNumber;
    ImageView imageView;
    private TextView text_guide_subject;
    private TextView text_guide_detail;

    static PageFragment newInstance(int pageNumber) {
        final PageFragment fragment = new PageFragment();

        final Bundle args = new Bundle();
        args.putInt(PAGE_NUM, pageNumber);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments() != null ? getArguments().getInt(PAGE_NUM) : -1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_page, container, false);
        imageView = (ImageView) rootView.findViewById(R.id.fragment_page_image);
        text_guide_subject = (TextView) rootView.findViewById(R.id.fragment_page_subject);
        text_guide_detail = ((TextView) rootView.findViewById(R.id.fragment_page_detail));
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final int imageResId = GuideActivity.imageResIds[mPageNumber];
        final int subjectStringResId = GuideActivity.subjectStringResIds[mPageNumber];
        final int detailStringResId = GuideActivity.detailStringResIds[mPageNumber];

        imageView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap guide_image = Utility.decodeBitmap(getResources(), imageResId, imageView.getMeasuredWidth(), imageView.getMeasuredHeight());
        String subject = getString(subjectStringResId);
        String detail = getString(detailStringResId);

        imageView.setImageBitmap(guide_image);
        text_guide_subject.setText(subject);
        text_guide_detail.setText(detail);

        text_guide_subject.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "SeoulNamsanB.otf"));
        text_guide_detail.setTypeface(Typeface.createFromAsset(getResources().getAssets(), "SeoulNamsanB.otf"));
    }
}
