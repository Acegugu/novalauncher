package com.nova.startlauncher.app.CardClass.Photo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class PhotoSelectActivity extends Activity implements View.OnClickListener{
    Context context;
    private static final int SELECT_PHOTO = 100;
    private ImageView [] imageViewList = new ImageView[4];
    private Button okButton;
    private PhotoItem[] imageList = new PhotoItem[4];
    private int state = -1, width;
    private LinearLayout backgroundLayout;
    private Boolean photoselect = false;
    private EasyTracker easyTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_select);
        context = getApplicationContext();
        init();
        setListener();
        setBackground();
        imageViewList[0].getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                width = imageViewList[0].getWidth();
                imageViewList[0].getViewTreeObserver().removeGlobalOnLayoutListener(this);
                SharedPreferences sharedPreferences = context.getSharedPreferences("photo", Context.MODE_PRIVATE);
                if (sharedPreferences.getBoolean("PhotoSelect", false)) loadPhotoItemList();
            }
        });


    }

    private void init()
    {
        easyTracker = EasyTracker.getInstance(context);

        imageViewList[0] = (ImageView)findViewById(R.id.photo_select_image1);
        imageViewList[1] = (ImageView)findViewById(R.id.photo_select_image2);
        imageViewList[2] = (ImageView)findViewById(R.id.photo_select_image3);
        imageViewList[3] = (ImageView)findViewById(R.id.photo_select_image4);
        okButton = (Button)findViewById(R.id.photo_select_ok);
        backgroundLayout = (LinearLayout)findViewById(R.id.photo_select_activity_layout);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!BuildConfig.DEBUG){
            EasyTracker.getInstance(this).activityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!BuildConfig.DEBUG){
            EasyTracker.getInstance(this).activityStop(this);
        }
    }

    private void setListener()
    {
        imageViewList[0].setOnClickListener(this);
        imageViewList[1].setOnClickListener(this);
        imageViewList[2].setOnClickListener(this);
        imageViewList[3].setOnClickListener(this);
        okButton.setOnClickListener(this);
    }

    private void setBackground()
    {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Setting", getApplicationContext().MODE_MULTI_PROCESS);
        String background = sharedpreferences.getString("SettingBackground", "");

        if(Build.VERSION.SDK_INT >= 16)
        {
            if(background.equals(""))
                backgroundLayout.setBackground(getResources().getDrawable(R.drawable.background1));
            else
                backgroundLayout.setBackground(getResources().getDrawable(getResources().getIdentifier(background,null,getPackageName())));
        }
        else {
            if (background.equals(""))
                backgroundLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1));
            else
                backgroundLayout.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background, null, getPackageName())));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == SELECT_PHOTO && data != null)
        {
            photoselect = true;
            getBitmapFromCameraData(data,context);
            try {
                bitmapScale(state);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!BuildConfig.DEBUG) {

                easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Photo Card Selection Done", Long.valueOf(8)).build());

            }
        }
        else if(resultCode == RESULT_CANCELED)
            state = -1;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo_select_image1:
                state = 0;
                break;
            case R.id.photo_select_image2:
                state = 1;
                break;
            case R.id.photo_select_image3:
                state = 2;
                break;
            case R.id.photo_select_image4:
                state = 3;
                break;
            case R.id.photo_select_ok:
                if (photoselect) {
                    SharedPreferences sharedPreferences = context.getSharedPreferences("photo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("PhotoSelect", true);
                    editor.commit();
                    PhotoCard.PhotoUpdateFlag = true;
                    savePhotoItemList(imageList);
                }
                finish();
                state = -1;
                break;
        }
        if (state != -1) {
            Intent selectImageIntent = new Intent(Intent.ACTION_PICK);
            selectImageIntent.setType("image/*");
            startActivityForResult(selectImageIntent, SELECT_PHOTO);
        }
    }

    private void savePhotoItemList(PhotoItem[] photoItems)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("photo",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Set<String> strings = new HashSet<String>();

        for(int i=0; i<4; i++)
        {
            if(imageList[i] != null) {
                JSONObject obj = new JSONObject();
                try {
                    obj.put("name", imageList[i].getName());
                    obj.put("date", imageList[i].getDate());
                    obj.put("location", imageList[i].getLocation());

                    strings.add(obj.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        editor.putStringSet("photoItemList",strings);
        editor.commit();
    }

    private void loadPhotoItemList()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("photo",Context.MODE_PRIVATE);
        Set<String> strings = sharedPreferences.getStringSet("photoItemList",null);

        int i=0;
        for(String s : strings){
            try{
                JSONObject jsonObject = new JSONObject(s);
                PhotoItem photoItem = new PhotoItem(jsonObject.getString("name"),jsonObject.getString("date"),jsonObject.getString("location"));
                imageList[i] = (photoItem);
                try {
                    bitmapScale(i);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i++;

            }catch(JSONException e){
                e.printStackTrace();
            }

        }

    }

    public void getBitmapFromCameraData(Intent data, Context context) {

        String[] projection = new String[]{
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.LATITUDE,
                MediaStore.Images.ImageColumns.LONGITUDE
        };

        Uri selectedImage = data.getData();
        Cursor cursor = context.getContentResolver().query(selectedImage, projection, null, null, null);
        if (cursor.moveToFirst()) {
            String location = "";
            if (cursor.getString(1) != null) {
                if (cursor.getString(1).equals("Camera")) {
                    if (cursor.getString(3) != null && cursor.getString(4) != null)
                        location = Utility.findAddress(context, Double.parseDouble(cursor.getString(3)), Double.parseDouble(cursor.getString(4)));

                    PhotoItem photoItem = new PhotoItem(cursor.getString(0), cursor.getString(2), location);
                    imageList[state] = photoItem;
                }
            } else {
                try {
                    if (cursor.getString(3) != null && cursor.getString(4) != null)
                        location = Utility.findAddress(context, Double.parseDouble(cursor.getString(3)), Double.parseDouble(cursor.getString(4)));

                    PhotoItem photoItem = new PhotoItem(cursor.getString(0), cursor.getString(2), location);
                    imageList[state] = photoItem;
                }catch(NullPointerException e)
                {
                    Toast.makeText(context,R.string.exception_photo_string,2000).show();
                }
            }
        }
        cursor.close();
    }

    public void bitmapScale(int state) throws IOException {
        Bitmap tempBitmap;

        int orientation_val = 0;
        int srcWidth = 0;
        float widthScale;

        try {
            ExifInterface exifInterface = new ExifInterface(imageList[state].getName());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            if (orientation == 6)
                orientation_val = 90;
            else if (orientation == 3)
                orientation_val = 180;
            else if (orientation == 8)
                orientation_val = 270;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(imageList[state].getName(), options);

            if (orientation_val == 90 || orientation_val == 270) {
                srcWidth = options.outHeight;
            } else {
                srcWidth = options.outWidth;
            }
            widthScale = srcWidth / width;
            options.inJustDecodeBounds = false;
            options.inSampleSize = (int) widthScale;
            tempBitmap = BitmapFactory.decodeFile(imageList[state].getName(), options);

            try {
                if (tempBitmap != null) {
                    if (orientation_val > 0) {
                        Matrix matrix = new Matrix();
                        matrix.postRotate(orientation_val);
                        tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0, tempBitmap.getWidth(), tempBitmap.getHeight(), matrix, true);
                    } else {
                        tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0, tempBitmap.getWidth(), tempBitmap.getHeight());
                    }
                    if (tempBitmap != null) {
                        imageViewList[state].setImageBitmap(tempBitmap);
                        imageViewList[state].setAlpha(1.0f);
                    }
                }
            }
            catch(OutOfMemoryError e){  }
        }
        catch(NullPointerException e)
        {
            Toast.makeText(context, R.string.exception_null_image, 2000).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.photo_select, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
