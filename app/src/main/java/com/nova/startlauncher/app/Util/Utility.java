package com.nova.startlauncher.app.Util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Deeraccun on 2014-04-06.
 */
public class Utility {
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        //int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            //listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    /** Resource Utility **/
    public static int getResource(Resources resources, String resourceName, String name) {
        int resId = resources.getIdentifier(name, resourceName, "com.nova.startlauncher.app");
        return resId;
    }

    /** Get Bitmap **/
    public static Bitmap decodeBitmap(Resources context, int id, int REQUIRED_WIDTH, int REQUIRED_HIGHT){
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context, id, o);

        //Find the correct scale value. It should be the power of 2.
        int scale=1;
        while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
            scale*=2;

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize=scale;
        o2.inPreferredConfig = Bitmap.Config.RGB_565;
        return BitmapFactory.decodeResource(context, id, o2);
    }

    /** Get Location String **/
    public static String findAddress(Context context, double lat, double lng) {
        String currentLocationAddress = "";
        Geocoder geocoder = new Geocoder(context, Locale.KOREA);
        List<Address> address;
        try {
            if (geocoder != null) {
                address = geocoder.getFromLocation(lat, lng, 1);

                if (address != null && address.size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    Address add = address.get(0);
                    if(add.getLocality()!=null)
                        sb.append(add.getLocality()).append(" ");
                    if(add.getThoroughfare()!=null)
                        sb.append(add.getThoroughfare());

                    currentLocationAddress = sb.toString();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return currentLocationAddress;
    }
}