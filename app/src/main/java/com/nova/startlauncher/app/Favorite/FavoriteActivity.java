package com.nova.startlauncher.app.Favorite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.AllApp.AllAppActivity;
import com.nova.startlauncher.app.Icon.AppIcon;

import com.google.analytics.tracking.android.EasyTracker;
import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.FrequentDBHelper;
import com.nova.startlauncher.app.Util.FrequenteAppModel;

import java.util.List;

public class FavoriteActivity extends Activity {

    private static final String titleFont = "SeoulNamsanB.otf";
    GridView frequentGrid;
    GridView favoriteGrid;
    FavoriteAdapter favoriteAdapter;

    TextView backHomeButton;
    TextView editButton;

    private Activity activity = this;
    private PackageManager packageManager;
    private FavoriteDBHelper favoritedb;
    List<FavoriteAppModel> favoriteAppList;

    AppIcon info = null;
    boolean deleteableState = false;

    private static final int CALL_FROM_FAVORITE_ACTIVITY = 1;

    private List<FrequenteAppModel> frequentlyUsedAppList;

    private LinearLayout favoriteAppsBackground;
    private LinearLayout favoriteBackground;
    private static EasyTracker easyTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        packageManager = getApplicationContext().getPackageManager();

        //자주 사용하는 애플리케이션을 DB에서 꺼내오기위한 소스 2줄
        FrequentDBHelper frequentDB = new FrequentDBHelper(getApplicationContext(), "FrequenteManager", null, 1);
        frequentlyUsedAppList = frequentDB.getAppsThreeList();

        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        save();
    }

    private void init() {
        easyTracker = EasyTracker.getInstance(getApplicationContext());
        gridViewDataSet();

        //뒤로 버튼을 눌러서 화면을 끄게 함. 이 기능은 없어져야함.
        backHomeButton = (TextView) findViewById(R.id.activity_favorite_back_text);
        backHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //편집 버튼.. 이 로직과 관련한 함수, 코드는 통째로 사용하는것을 추천함.
        editButton = (TextView) findViewById(R.id.activity_favorite_favorite_edit_text);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (favoriteAppList.size() == 0) {
                    Toast.makeText(getApplicationContext(), "편집할 애플리케이션이 없습니다.\n 더하기 버튼을 눌러서 앱을 추가해주세요", 1000).show();
                } else {
                    if (!deleteableState) {
                        deleteModeOn();
                    } else {
                        deleteModeOff();
                    }
                    deleteableState = !deleteableState;
                }
            }
        });

        //각종 TextView 초기화
        favoriteAppsBackground = (LinearLayout) findViewById(R.id.activity_favorite_top_background);
        favoriteBackground = (LinearLayout) findViewById(R.id.activity_favorite);

        TextView favoriteBackText = (TextView) findViewById(R.id.activity_favorite_back_text);
        TextView favoriteAppsTitle = (TextView) findViewById(R.id.activity_favorite_favorite_apps_text);
        TextView favoriteEditText = (TextView) findViewById(R.id.activity_favorite_favorite_edit_text);
        TextView frequentTitle = (TextView) findViewById(R.id.activity_favorite_frequent_title);

        favoriteBackText.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));
        favoriteAppsTitle.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));
        favoriteEditText.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));
        frequentTitle.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));

        //Shared Preference를 통해 배경화면 설정에 맞춰서 배경화면을 바꿔주는 로직. 건들필요없음.
        BackgroundChange();
    }

    private void BackgroundChange() {
        SharedPreferences backgroundPref = getApplicationContext().getSharedPreferences("Setting", getApplicationContext().MODE_MULTI_PROCESS);
        String background = backgroundPref.getString("SettingBackground", "");

        if (Build.VERSION.SDK_INT >= 16) {
            if (background.equals("")) {
                favoriteAppsBackground.setBackground(getResources().getDrawable(R.drawable.background1_title));
                favoriteBackground.setBackground(getResources().getDrawable(R.drawable.background1_back));

            } else {
                favoriteAppsBackground.setBackground(getResources().getDrawable(getResources().getIdentifier(background + "_title", null, getPackageName())));
                favoriteBackground.setBackground(getResources().getDrawable(getResources().getIdentifier(background + "_back", null, getPackageName())));
            }

        } else {
            if (background.equals("")) {
                favoriteAppsBackground.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1_title));
                favoriteBackground.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1_back));
            } else {
                favoriteAppsBackground.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background + "_title", null, getPackageName())));
                favoriteBackground.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background + "_back", null, getPackageName())));
            }
        }
    }

    public void gridViewDataSet() {
        //즐겨찾기 : 유저가 수동으로 정해놓은 앱들을 보여주는곳, 추천앱 : 서울버스, 카카오톡, 지하철앱을 GridView를 통해 띄우는 기초적인 로직입니다.
        //즐겨찾기 시작
        favoritedb = new FavoriteDBHelper(getApplicationContext(), "FavoriteManager", null, 1);
        favoriteAppList = favoritedb.getFavoriteAppsList();
        favoriteGrid = (GridView) findViewById(R.id.activity_favorite_apps);
        favoriteAdapter = new FavoriteAdapter();
        favoriteGrid.setAdapter(favoriteAdapter);

        // 자주 쓰는 앱 시작
        frequentGrid = (GridView) findViewById(R.id.activity_favorite_frequent_apps_gridview);
        frequentGrid.setAdapter(new FrequentAdapter());
    }

    //편집모드에서 앱을 삭제하는 로직. 왠만하면 건들지 않는것이 정신건강에 이로울듯.
    public void deleteModeOn() {
        if (favoriteAppList.size() != 0) {
            for (int i = 0; i < favoriteAppList.size(); i++) {
                View view = favoriteGrid.getChildAt(i);
                ImageView deleteButtonX = (ImageView) view.findViewById(R.id.icon_item_delete);
                ImageView AppIcon = (ImageView) view.findViewById(R.id.dock_item_icon_image);
                AppIcon.setClickable(false);
                deleteButtonX.setVisibility(View.VISIBLE);
                final int finalI = i;
                deleteButtonX.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        favoriteAppList.remove(favoriteAppList.get(finalI));
                        deleteModeOff();
                        favoriteAdapter.notifyDataSetChanged();
                        if (favoriteAppList.size() == 0) {
                            deleteModeOff();
                            deleteableState = !deleteableState;
                        } else if (favoriteAppList.size() > 0) {
                            deleteModeOn();
                        }
                    }
                });
            }
        }
    }

    //편집모드에서 비편집 모드로 돌아오는 로직. 왠만하면 건들지 않는것이 정신건강에 이로울듯.
    public void deleteModeOff() {
        int size = favoriteAppList.size() == 9 ? 9 : favoriteAppList.size() + 1;
        for (int i = 0; i < size; i++) {
            View view = favoriteGrid.getChildAt(i);
            ImageView deleteButtonX = (ImageView) view.findViewById(R.id.icon_item_delete);
            deleteButtonX.setVisibility(View.GONE);
            ImageView AppIcon = (ImageView) view.findViewById(R.id.dock_item_icon_image);
            AppIcon.setClickable(true);
        }
    }

    //코드를 작성한 사람이 더 잘알만한 그 코드
    public void save() {
        favoritedb.setFavoriteAppsList(favoriteAppList);
    }

    //즐겨찾기 앱을 GridView에 뿌려주는 Adapter로직. 이것 자체를 통째로 옮겨서 쓰는것을 추천함.
    private class FavoriteAdapter extends BaseAdapter {

        LayoutInflater inflater;

        private FavoriteAdapter() {
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            if (favoriteAppList.size() < 9) {
                return favoriteAppList.size() + 1;
            } else {
                return favoriteAppList.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return favoriteAppList.get(position);
        }

        @Override
        public final long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.icon_item, parent, false);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.dock_item_icon_image);
            TextView textView = (TextView) convertView.findViewById(R.id.dock_item_icon_text);

            //애플리케이션 아이콘을 뿌리기위한 로직. 여기에 흰색상자모양인 추가버튼을 뿌리는 로직은 없다.
            if ((position + 1 <= favoriteAppList.size())) {

                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dpToPx(55), dpToPx(55));
                imageView.setLayoutParams(layoutParams);

                try {
                    info = new AppIcon(favoriteAppList.get(position).getPackageName(), getApplicationContext());
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                imageView.setImageDrawable(info.getAppIcon());
                textView.setText(info.getAppTitle());
                String strColor = "#ffffff";
                textView.setTextColor(Color.parseColor(strColor));

                imageView.setOnClickListener(new Button.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = packageManager.getLaunchIntentForPackage(favoriteAppList.get(position).getPackageName());
                        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                });
            }

            //흰색상자모양의 추가버튼을 넣는 로직.
            if (position + 1 > favoriteAppList.size() && favoriteAppList.size() < 9) {

                imageView.setImageDrawable(getResources().getDrawable(R.drawable.add_app));
                textView.setText("");

                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override

                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), AllAppActivity.class);
                        intent.putExtra("callStage", CALL_FROM_FAVORITE_ACTIVITY);
                        startActivity(intent);
                    }
                });
            }

            return convertView;
        }
    }

    class ViewHolder {
        ImageView appIcon = null;
        ImageView appIconDelete = null;
        TextView appTitle = null;
    }

    //자주사용하는 앱을 GridView에 띄우기 위한 Adapter
    private class FrequentAdapter extends BaseAdapter {

        LayoutInflater inflater;

        private FrequentAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return frequentlyUsedAppList.size();
        }

        @Override
        public Object getItem(int position) {
            return frequentlyUsedAppList.get(position);
        }

        @Override
        public final long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.icon_item, parent, false);
                viewHolder.appIcon = (ImageView) convertView.findViewById(R.id.dock_item_icon_image);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dpToPx(55), dpToPx(55));
                viewHolder.appIcon.setLayoutParams(layoutParams);
                viewHolder.appIconDelete = (ImageView) convertView.findViewById(R.id.icon_item_delete);
                viewHolder.appTitle = (TextView) convertView.findViewById(R.id.dock_item_icon_text);
                viewHolder.appTitle.setTextSize(17f);
                viewHolder.appTitle.setTextColor(Color.WHITE);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            try {
                info = new AppIcon(frequentlyUsedAppList.get(position).getPackageName(), getApplicationContext());
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            viewHolder.appIcon.setImageDrawable(info.getAppIcon());
            viewHolder.appTitle.setText(info.getAppTitle());
            viewHolder.appIcon.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //설치된 모든 앱 보기의 로직과 같다.
                    Intent intent = packageManager.getLaunchIntentForPackage(frequentlyUsedAppList.get(position).getPackageName());
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

                    if (!BuildConfig.DEBUG) {
                        if (!BuildConfig.DEBUG) {
                            easyTracker.send(MapBuilder.createEvent("Press Icon", "Icon Press", "FrequentlyAppClick", Long.valueOf(15)).build());
                        }
                    }
                }
            });

            return convertView;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        gridViewDataSet();
        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStart(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!BuildConfig.DEBUG) {
            EasyTracker.getInstance(this).activityStop(this);
        }
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}