package com.nova.startlauncher.app.Icon;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;

import java.util.Locale;

/**
 * Created by FlaShilver on 2014. 4. 16..
 */
public class AppIcon {

    private Drawable appIcon;
    private Boolean iscallIcon = false;
    private CharSequence appTitle;
    public Context context;
    private String appPackage;
    private ApplicationInfo applicationInfo;

    public AppIcon(String appPackage, Context context) throws PackageManager.NameNotFoundException {
        this.appPackage = appPackage;
        this.context = context;

        setApplicationInfo();
        setAppIcon();
        setAppTitle();
    }

    public AppIcon(Context context) {
        this.context = context;
    }

    public void setApplicationInfo() throws PackageManager.NameNotFoundException {
        this.applicationInfo = context.getPackageManager().getApplicationInfo(appPackage, PackageManager.GET_META_DATA);
    }

    public void setAppTitle() throws PackageManager.NameNotFoundException {
        Configuration config = new Configuration();
        config.locale = new Locale("ko");
        Resources appRes = context.getPackageManager().getResourcesForApplication(applicationInfo.packageName.toString());
        appRes.updateConfiguration(config, context.getResources().getDisplayMetrics());
        appTitle = applicationInfo.loadLabel(context.getPackageManager());
    }

    public Drawable getAppIcon() {
        return appIcon;
    }

    public void setAppIcon() {
        this.appIcon = applicationInfo.loadIcon(context.getPackageManager());
    }

    public CharSequence getAppTitle() {
        return appTitle;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setisCallIcon(Boolean flag)
    {
        iscallIcon = flag;
    }

    public void Play(View v, Activity activity) {

    }

    public void Play() {
        Intent intent = null;

        if(iscallIcon)
            intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"));
        else
            intent = context.getPackageManager().getLaunchIntentForPackage(getAppPackage());

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }


}
