package com.nova.startlauncher.app.CardClass.Search;

import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.net.Uri;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;


/**
 * 검색 카드
 */
public class SearchCard extends Card implements View.OnClickListener {
    ImageButton searchButton;
    EditText searchText;
    private boolean isKeyboardUp = false;

    public SearchCard(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);
        init();
        setListener();
    }

    public void init() {
        searchButton = (ImageButton) cardView.findViewById(R.id.search_card_button);
        searchText = (EditText) cardView.findViewById(R.id.search_card_text);
    }

    @Override
    public void setListener() {

        searchButton.setOnClickListener(this);
        searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    //Log.d("keyboard", "on focus");
                    searchText.setHint("");
                } else {
                    //Log.d("keyboard", "off focus");
                    searchText.setHint(R.string.search_card_hint);
                }
            }
        });
        searchText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Log.d("keyboard", "on touch");
                showKeyboard();
                return true;
            }
        });
        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        search();
                        break;
                    default:
                        return false;
                }

                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        EasyTracker easyTracker = EasyTracker.getInstance(context);

        switch (v.getId()) {
            case R.id.search_card_button:
                search();
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "PhotoClick", Long.valueOf(22)).build());

                }
            break;
        }
    }

    private void search() {
        String text = searchText.getText().toString();
        if (text != null && !text.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://search.naver.com/search.naver?query=" + text));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public void searchEditRequestFocus() {
        searchText.requestFocus();
    }

    private void showKeyboard() {
        if (!isKeyboardUp) {
            //Log.d("keyboard", "show keyboard");
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            searchText.requestFocus();
            inputMethodManager.showSoftInput(searchText, 0);
            isKeyboardUp = true;
        }
    }


    public void hideKeyboard() {
        if (isKeyboardUp) {
            //Log.d("keyboard", "hide keyboard");
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
            isKeyboardUp = false;
            searchText.clearFocus();
        }
    }

    public void cleanEditText() {
        searchText.setText("");
    }
}
