package com.nova.startlauncher.app.CardClass.Setting;

import android.app.ActivityManager;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minor5pm on 2014-05-22.
 */
public class TaskCleaner {
    private ActivityManager am;
    private Context context;

    public TaskCleaner(Context context) {
        this.context = context;
        this.am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public List<String> getCleanableTaskPackageNames() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcessInfos = am.getRunningAppProcesses();
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = am.getRunningTasks(Integer.MAX_VALUE);
        List<String> cleanableProcessNames = new ArrayList<String>();

        for(ActivityManager.RunningTaskInfo runningTaskInfo: runningTaskInfos) {
            String processName = runningTaskInfo.baseActivity.getPackageName();

            for(ActivityManager.RunningAppProcessInfo runningAppProcessInfo: runningAppProcessInfos) {
                if(runningAppProcessInfo.processName.equals(processName)) {
                    if(runningAppProcessInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
                        cleanableProcessNames.add(processName);
                        break;
                    }
                }
            }
        }

        return cleanableProcessNames;
    }

    public int countCleanableTasks() {
        return getCleanableTaskPackageNames().size();
    }

    public int cleanTasks() {
        List<String> cleanableTaskPackageNames = getCleanableTaskPackageNames();

        int count = 0;
        for(String packageName: cleanableTaskPackageNames) {
            am.killBackgroundProcesses(packageName);
            count++;
        }
        return count;
    }
}
