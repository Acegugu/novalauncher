package com.nova.startlauncher.app.CardClass.Cost;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.net.TrafficStats;
import android.widget.TextView;

import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;

import java.util.Calendar;

/**
 * Created by kmkyoung on 2014. 4. 3..
 */
public class CostCard extends Card {
    private final String TAG = "CostCard";
    static final int REGISTER_CLIENT = 0;
    static final int SEND_SMS_UPDATE = 5;
    static final int SEND_DATA_UPDATE = 6;
    static final int SEND_CALL_UPDATE = 7;

    private CostCustomView callCircle, smsCircle, dataCircle;
    private TextView callText, smsText, dataText;

    private long costUsingData;
    private int costUsingSms, costUsingCall;

    private CostCardReceiver costcardreceiver = new CostCardReceiver();
    private Messenger serviceMessenger;
    private final Messenger receiveMessenger = new Messenger(new IncomingHandler());

    private TrafficStats trafficstatus = new TrafficStats();

    private ServiceConnection serviceConnection = new ServiceConnection(){
        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceMessenger = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceMessenger = new Messenger( service );

            try{
                Message msg = Message.obtain( null, REGISTER_CLIENT );
                msg.replyTo = receiveMessenger;
                serviceMessenger.send( msg );
            }
            catch( RemoteException e ){  }
        }
    };

    public CostCard(int layout, String cardName, Context context, int cardId)
    {
        super(layout,cardName,context,cardId);
        init();
        setListener();

        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost", Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        if(sharedpreferences.getBoolean("CostFirstInstall",true))
        {
            initCostData();
            editor.putBoolean("CostFirstInstall",false);
        }
        else {
            // device restart
            if (sharedpreferences.getBoolean("CostRebootFlag", false)) {
                editor.putLong("CostInitData", trafficstatus.getMobileRxBytes() + trafficstatus.getMobileTxBytes());
                editor.putLong("CostPreData", sharedpreferences.getLong("CostBackupData", 0));
                editor.putBoolean("CostRebootFlag", false);
            }
        }

        editor.commit();
        updateCostCircle();

        IntentFilter intentfilter = new IntentFilter("android.intent.action.ACTION_SHUTDOWN");
        intentfilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentfilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        context.registerReceiver(costcardreceiver, intentfilter);

        context.bindService(new Intent(context, CostCardService.class), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void init()
    {
        callCircle = (CostCustomView)cardView.findViewById(R.id.cost_callcircle);
        smsCircle = (CostCustomView)cardView.findViewById(R.id.cost_smscircle);
        dataCircle = (CostCustomView)cardView.findViewById(R.id.cost_datacircle);
        callText = (TextView)cardView.findViewById(R.id.cost_calltext);
        smsText = (TextView)cardView.findViewById(R.id.cost_smstext);
        dataText = (TextView)cardView.findViewById(R.id.cost_datatext);

        callCircle.setState(0);
        callCircle.setMaxCount(60*60);           //1 hour
        smsCircle.setState(1);
        smsCircle.setMaxCount(50);              //50
        dataCircle.setState(2);
        dataCircle.setMaxCount((long)1024*1024*100);  //100MB

    }

    public void updateCostCircle()
    {
        updateUsingCall();
        updateUsingSMS();
        updateUsingData();
    }

    public void initCostData()
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("CostUsingCall",0);
        editor.putInt("CostUsingSms", 0);
        editor.putLong("CostInitData",trafficstatus.getMobileRxBytes()+trafficstatus.getMobileTxBytes());
        editor.putLong("CostUsingData",0);
        editor.putLong("CostPreData",0);
        editor.putLong("CostBackupData",0);
        editor.commit();
    }

    public void updateUsingSMS()
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
        costUsingSms = sharedpreferences.getInt("CostUsingSms", 0);
        smsCircle.setCurrentCount(costUsingSms);
        smsCircle.invalidate();
        smsText.setText(costUsingSms+"건");
    }

    public void updateUsingCall()
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
        costUsingCall = sharedpreferences.getInt("CostUsingCall", 0);
        callCircle.invalidate();
        callCircle.setCurrentCount(costUsingCall);
        callText.setText(costUsingCall/60+"분");
    }

    public void updateUsingData()
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
        costUsingData = sharedpreferences.getLong("CostUsingData", 0);
        dataCircle.setCurrentCount(costUsingData);
        dataCircle.invalidate();
        dataText.setText(costUsingData/1024/1024+"MB");
    }

    public void onDestroy()
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putLong("CostBackupData",sharedpreferences.getLong("CostUsingData",0));
        editor.commit();
        context.unregisterReceiver(costcardreceiver);
    }

    public class IncomingHandler extends Handler{
        @Override
        public void handleMessage( Message msg ){
            switch (msg.what)
            {
                case SEND_DATA_UPDATE:
                    updateUsingData();
                    break;
                case SEND_SMS_UPDATE:
                    updateUsingSMS();
                    break;
                case SEND_CALL_UPDATE:
                    updateUsingCall();
                    break;
            }
        }
    }

    public class CostCardReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if("android.intent.action.ACTION_SHUTDOWN".equals(intent.getAction()))
            {
                SharedPreferences sharedpreferences = context.getSharedPreferences("Cost",context.MODE_MULTI_PROCESS);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("CostRebootFlag",true);
                editor.commit();
            }
            else if (intent.getAction().equals(Intent.ACTION_TIME_CHANGED) || intent.getAction().equals(Intent.ACTION_TIMEZONE_CHANGED)) {
                Calendar calendar = Calendar.getInstance();
                if(calendar.get(Calendar.DAY_OF_MONTH) == 1)
                    initCostData();
            }
        }
    }

//      Request 날리는 클래스
//    public class ConnectTest extends AsyncTask<Void,Void,Void>
//    {
//        ConnectivityManager connectivitymanager;
//        NetworkInfo networkinfo;
//        URL url;
//        boolean isWifiFree = false;
//
//        @Override
//        protected void onPreExecute()
//        {
//            connectivitymanager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//            networkinfo = connectivitymanager.getActiveNetworkInfo();
//
//        }
//
//        @Override
//        protected Void doInBackground(Void... params)
//        {
//            if (networkinfo != null && networkinfo.isConnected()) {
//
//                try {
//                    url = new URL("http://www.google.com");
//                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
//                    urlc.setRequestProperty("Connection", "close");
//                    urlc.setRequestMethod("GET");
//                    urlc.setConnectTimeout(5 * 1000);
//                    urlc.connect();
//
//                    if (urlc.getResponseCode() == 200) {
//                        isWifiFree = true;
//                    } else {
//                        isWifiFree = true;
//                    }
//                } catch (IOException e) {
//                    isWifiFree = false;
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onProgressUpdate(Void... params)
//        {
//
//        }
//
//        @Override
//        protected void onPostExecute(Void res)
//        {
//            if(isWifiFree)
//                wifiFreeImage.setVisibility(View.VISIBLE);
//            else
//                wifiFreeImage.setVisibility(View.INVISIBLE);
//
//        }
//
//
//    }
}
