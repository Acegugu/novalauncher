package com.nova.startlauncher.app.CardClass.NewCalendar;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.ibm.icu.util.ChineseCalendar;
import com.nova.startlauncher.app.CardLauncherActivity;
import com.nova.startlauncher.app.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by FlaShilver on 2014. 6. 9..
 */
public class NewCalendarDayModel {
    private int day; //요일 1 : 일요일 ~ 7 : 토요일
    private int weatherState; //해당 날의 날씨 상태    //0 : 맑음 / 1 : 흐림 / 2 : 맑은뒤 비내림 / 3: 비
    //음력날짜를 모델로 어떻게 줄것인가?
    private Calendar dayCalendar;
    private String todaySolorDate; // 해당 하루의 날짜 정보 // Calender나 Date로 나타낼 생각.
    private String todayLunarDate;
    private int skycodeday;
    private Context context;
    private static StringBuilder html;


    public NewCalendarDayModel(int day, Calendar dayCalendar, Context context) {
        this.day = day;
        this.dayCalendar = dayCalendar;
        this.context = context;

        setSolarDate();
        setLunarDate();
        getWeather(dayCalendar);
    }

    public static void setHtml(StringBuilder html) {
        NewCalendarDayModel.html = html;
    }

    // 해당 날짜의 날씨를 가져옴
    // 오늘을 포함한 5일까지의 간단한 날씨 상태, 최고, 최저온도, 이미지 코드를 지원함
    public void getWeather(Calendar date) {
        if (checkInternetConnection()) {
            WeatherAsyncTask thread = new WeatherAsyncTask(date);
            thread.execute();
        }else{
            weatherState=0;
        }
    }

    /**
     * Network, Xml Parsing으로 날씨 목록을 만들고 UI Update하는 AsyncTask
     */
    class WeatherAsyncTask extends AsyncTask<Void, Void, ArrayList<Weather>> {
        private final String url = "http://weather.service.msn.com/data.aspx?weadegreetype=C&culture=ko-KR&weasearchstr=Seoul";
        private Calendar date;
        private ArrayList<Weather> weathers;

        public WeatherAsyncTask(Calendar date) {
            this.date = (Calendar) date.clone();
        }

        @Override
        protected ArrayList<Weather> doInBackground(Void... params) {
            Calendar today = Calendar.getInstance();
            if (html == null) {
                if (weathers == null || (today.get(Calendar.DAY_OF_YEAR) != weathers.get(0).date.get(Calendar.DAY_OF_YEAR))) {
                    StringBuilder html = new StringBuilder();
                    HttpURLConnection conn = null;
                    try {
                        URL url = new URL(this.url);
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setUseCaches(false);
                        conn.setRequestProperty("Cache-Control", "no-cache");
                        conn.setRequestProperty("Content-Type", "application/xml");
                        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            String line;
                            while ((line = br.readLine()) != null) {
                                html.append(line + "\n");
                            }
                            br.close();
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        conn.disconnect();
                    }
                    setHtml(html);
                    weathers = parseXml(html.toString());
                }
            } else {
                weathers = parseXml(html.toString());
            }

            return weathers;
        }

        @Override
        public void onPostExecute(ArrayList<Weather> weathers) {
            if (weathers != null) {
                for (Weather weather : weathers) {
                    if (weather.date.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)) {
                        setWeatherState(weather.skycodeday);
                    }
                }
            }
        }

        private ArrayList<Weather> parseXml(String result) {
            if (result == null || result.isEmpty())
                return null;

            ArrayList<Weather> weathers = new ArrayList<Weather>();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                InputStream istream = new ByteArrayInputStream(result.getBytes("utf-8"));
                Document doc = builder.parse(istream);

                Element root = doc.getDocumentElement();

                NodeList itemList = root.getElementsByTagName("forecast");

                for (int i = 0; i < itemList.getLength(); i++) {
                    NamedNodeMap nodeAttrs = itemList.item(i).getAttributes();

                    Calendar date = Calendar.getInstance();
                    date.setTime(df.parse(nodeAttrs.getNamedItem("date").getNodeValue()));
                    String state = nodeAttrs.getNamedItem("skytextday").getNodeValue();
                    int low = Integer.valueOf(nodeAttrs.getNamedItem("low").getNodeValue());
                    int high = Integer.valueOf(nodeAttrs.getNamedItem("high").getNodeValue());
                    int skycodeday = Integer.valueOf(nodeAttrs.getNamedItem("skycodeday").getNodeValue());

                    weathers.add(new Weather(date, state, low, high, skycodeday));
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return weathers;
        }
    }

    /**
     * 인터넷이 연결되어 있는지 확인 *
     */
    private boolean checkInternetConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected();
    }

    public int getDayDrawable() {
        switch (this.day) {
            case 1:
                return R.drawable.sun;
            case 2:
                return R.drawable.mon;
            case 3:
                return R.drawable.tues;
            case 4:
                return R.drawable.wedn;
            case 5:
                return R.drawable.thus;
            case 6:
                return R.drawable.fri;
            case 7:
                return R.drawable.sat;
        }
        return 0;
    }

    public void setLunarDate() {

    }

    public void setSolarDate() {


        ChineseCalendar instance = CardLauncherActivity.chineseCalendar;

        instance.setTimeInMillis(dayCalendar.getTimeInMillis());
        todaySolorDate = "양 " + (dayCalendar.get(Calendar.MONTH) + 1) + "월 " + (dayCalendar.get(Calendar.DATE)) + "일"+" (음 " + (instance.get(com.ibm.icu.util.Calendar.MONTH) + 1) + "월 " + (instance.get(com.ibm.icu.util.Calendar.DATE)) + "일)";

    }

    public String getTodaySolorDate() {
        return todaySolorDate;
    }

    public String getTodayLunarDate() {
        return todayLunarDate;
    }

    public void setWeatherState(int skycodeday) {

        if (skycodeday == 4 || skycodeday == 9 || skycodeday == 11) //  4 == T자형 폭풍, 소나기
            weatherState = 3;
        else if (skycodeday == 28 || skycodeday == 30 || skycodeday == 34) // 대체로 흐림, 갬, 약간 흐림
            weatherState = 1;
        else if (skycodeday == 32) // 구름 없음
            weatherState = 0;
        else if (skycodeday == 39) // 소나기 후 맑음
            weatherState = 2;
    }

    public int getWeatherResource() {

        if (weatherState == 0) {
            return R.drawable.weather_sunny;
        } else if (weatherState == 1) {
            return R.drawable.weather_cloudy;
        } else if (weatherState == 2) {
            return R.drawable.weather_sunny_after_rain;
        } else if (weatherState == 3) {
            return R.drawable.weather_rain;
        }
        return 4;
    }
}
