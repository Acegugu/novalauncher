package com.nova.startlauncher.app.CardClass.Watch;

import android.widget.TextView;

/**
 * Created by kmkyoung on 2014. 5. 6..
 */
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.AttributeSet;

import java.util.Calendar;

public class WatchDigitalClock extends TextView {

    Calendar mCalendar;
    private final static String time = "h:mm";
    private final static String date = "yyyy. MM. dd";
    private final static String ampm = "a";
    private FormatChangeObserver mFormatChangeObserver;

    private Runnable mTicker;
    private Handler mHandler;
    private int dateFlag = -1;

    private boolean mTickerStopped = false;

    String mFormat;

    public WatchDigitalClock(Context context) {
        super(context);
        initClock(context);
    }

    public WatchDigitalClock(Context context, AttributeSet attrs) {
        super(context, attrs);
        initClock(context);
    }

    public void initClock(Context context) {

        if (mCalendar == null) {
            mCalendar = Calendar.getInstance();
        }

        mFormatChangeObserver = new FormatChangeObserver();
        getContext().getContentResolver().registerContentObserver(
                Settings.System.CONTENT_URI, true, mFormatChangeObserver);

    }

    @Override
    protected void onAttachedToWindow() {
        mTickerStopped = false;
        super.onAttachedToWindow();
        mHandler = new Handler();

        mTicker = new Runnable() {
            public void run() {
                if (mTickerStopped) return;
                mCalendar.setTimeInMillis(System.currentTimeMillis());
                setText(DateFormat.format(mFormat, mCalendar));
                invalidate();
                long now = SystemClock.uptimeMillis();
                long next = now + (1000 - now % 1000);
                mHandler.postAtTime(mTicker, next);
            }
        };
        mTicker.run();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mTickerStopped = true;
    }

    public void setFormat(int dateflag) {
        dateFlag = dateflag;
        if (dateFlag == 0) {
            mFormat = date;
        } else if (dateFlag == 1) {
            mFormat = time;
        } else if (dateFlag == 2) {
            mFormat = ampm;
        }

    }

    private class FormatChangeObserver extends ContentObserver {
        public FormatChangeObserver() {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            setFormat(dateFlag);
        }
    }

}