package com.nova.startlauncher.app.CardClass.NewContactBook;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 연락처 Model
 */
public class Contact implements Parcelable {
    public String id;
    public String name;
    public String phone_number;
    public int photo_id;

    public Contact(String id, String name, String phone_number, int photo_id) {
        this.id = id;
        this.name = name;
        this.phone_number = phone_number;
        this.photo_id = photo_id;
    }

    public Contact(Parcel in) {
        id = in.readString();
        name = in.readString();
        phone_number = in.readString();
        photo_id = in.readInt();
    }

    public static final Parcelable.Creator<Contact> CREATOR
            = new Parcelable.Creator<Contact>() {
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(phone_number);
        dest.writeInt(photo_id);
    }
}
