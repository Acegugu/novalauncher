package com.nova.startlauncher.app.CardClass.AllApp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.Favorite.FavoriteDBHelper;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.Util.RecommendAppModel;
import com.nova.startlauncher.app.R;

import java.util.ArrayList;
import java.util.List;

public class AllAppActivity extends Activity {

    private static final String titleFont = "SeoulNamsanB.otf";
    private PackageManager packageManager;
    private List<ApplicationInfo> apps;
    List<RecommendAppModel> recommendAppList;
    private int callStage;
    private static EasyTracker easyTracker;
    private TextView allapp_title;
    private LinearLayout backgroundLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_app);

        //이 모든앱보기 액티비티가 카드런처액티비티에서 call되었는지, 즐겨찾기 액티비티에서 추가버튼을 눌러서 call되었는지 알수있음.
        Intent getIntent = getIntent();
        callStage = getIntent.getExtras().getInt("callStage");

        easyTracker = EasyTracker.getInstance(getApplicationContext());

        init();
    }

    @Override
    public void onStart() {
        super.onStart();

        setRecommandAppList();

        if (!BuildConfig.DEBUG)
            EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!BuildConfig.DEBUG)
            EasyTracker.getInstance(this).activityStop(this);
    }

    private void init() {
        //findViewById를 최소화 하기위해 수정함.

        packageManager = getApplicationContext().getPackageManager();
        apps = getAllInstalledApplications(getApplicationContext());

        backgroundLayout = (LinearLayout) findViewById(R.id.activity_all_app);

        GridView allAppGridView = (GridView) findViewById(R.id.activity_all_app_area);
        allAppGridView.setAdapter(new AllAppAdapter());

        recommendAppList = getRecommendAppList();
        final GridView recommendGridView = (GridView) findViewById(R.id.activity_all_app_recommend_apps_gridview);
        recommendGridView.setAdapter(new RecommandAdapter());
        recommendGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (callStage == 0) {
                    // 다이얼로그 띄우기
                    if(recommendAppList.get(position).isInstallState() == false) {
                        DialogFragment newFragment = InstructionDialogFragment.newInstance(recommendAppList.get(position));
                        newFragment.show(getFragmentManager(), "dialog");
                    }
                    else
                    {
                        Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(recommendAppList.get(position).getPackageName());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(intent);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "아래의 항목에서 추가해주세요", 1000).show();
                }
            }
        });

        TextView recommendTitle = (TextView) findViewById(R.id.activity_all_app_recommend_title);
        recommendTitle.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));

        allapp_title = (TextView) findViewById(R.id.activity_all_app_all_text);
        allapp_title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), titleFont));

        BackgroundChange();

        GoogleAnalytics.getInstance(this).getTracker("UA-50706998-1");
    }

    //Shared Preference를 통해 배경화면 설정에 맞춰서 배경화면을 바꿔주는 로직. 건들필요없음.
    private void BackgroundChange() {
        SharedPreferences backgroundPref = getApplicationContext().getSharedPreferences("Setting", getApplicationContext().MODE_MULTI_PROCESS);
        String background = backgroundPref.getString("SettingBackground", "");

        if (Build.VERSION.SDK_INT >= 16) {
            if (background.equals("")) {
                allapp_title.setBackground(getResources().getDrawable(R.drawable.background1_title));
                backgroundLayout.setBackground(getResources().getDrawable(R.drawable.background1_back));

            } else {
                allapp_title.setBackground(getResources().getDrawable(getResources().getIdentifier(background + "_title", null, getPackageName())));
                backgroundLayout.setBackground(getResources().getDrawable(getResources().getIdentifier(background + "_back", null, getPackageName())));
            }

        } else {
            if (background.equals("")) {
                allapp_title.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1_title));
                backgroundLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.background1_back));
            } else {
                allapp_title.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background + "_title", null, getPackageName())));
                backgroundLayout.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier(background + "_back", null, getPackageName())));

            }
        }
    }

    //설치된 모든 애플리케이션을 불러오는 메소드 로직
    public List<ApplicationInfo> getAllInstalledApplications(Context context) {
        List<ApplicationInfo> installedApps = context.getPackageManager().getInstalledApplications(PackageManager.PERMISSION_GRANTED);
        List<ApplicationInfo> launchableInstalledApps = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo installedApp : installedApps) {
            if (context.getPackageManager().getLaunchIntentForPackage(installedApp.packageName) != null) {
                launchableInstalledApps.add(installedApp);
            }
        }

        try {
            //삼성의 갤러리앱을 불러오기위해...
            if (packageManager.getApplicationInfo("com.sec.android.gallery3d", PackageManager.GET_META_DATA) != null && (packageManager.getApplicationInfo("com.android.phone", PackageManager.GET_META_DATA)) != null) {

                launchableInstalledApps.add(packageManager.getApplicationInfo("com.sec.android.gallery3d", PackageManager.GET_META_DATA));
                launchableInstalledApps.add(packageManager.getApplicationInfo("com.android.phone", PackageManager.GET_META_DATA));

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return launchableInstalledApps;
    }

    //모든 설치된 앱을 띄우는 Adapter
    private class AllAppAdapter extends BaseAdapter {

        LayoutInflater inflater;

        private AllAppAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return apps.size();
        }

        @Override
        public Object getItem(int position) {
            return apps.get(position);
        }

        @Override
        public final long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            //GridView가 많아서 ViewHoler로직으로 최적화를 시도해봄.
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = inflater.inflate(R.layout.icon_item, parent, false);
                viewHolder.appIcon = (ImageView) convertView.findViewById(R.id.dock_item_icon_image);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dpToPx(55), dpToPx(55));
                viewHolder.appIcon.setLayoutParams(layoutParams);
                viewHolder.appIconDelete = (ImageView) convertView.findViewById(R.id.icon_item_delete);
                viewHolder.appTitle = (TextView) convertView.findViewById(R.id.dock_item_icon_text);
                viewHolder.appTitle.setTextSize(17f);
                viewHolder.appTitle.setTextColor(Color.WHITE);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //ViewHoler 로직 끝.


            final ApplicationInfo info = apps.get(position);

            viewHolder.appIcon.setImageDrawable(info.loadIcon(getPackageManager()));
            viewHolder.appTitle.setText(info.loadLabel(packageManager).toString());
            viewHolder.appIcon.setOnClickListener(new Button.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //카드런처액티비티에서 모든앱보기를 열었을때의 아이템 클릭 로직. 앱을 연다.
                    if (callStage == 0) {
                        Intent intent = packageManager.getLaunchIntentForPackage(info.packageName);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    } else { //즐겨찾기에서 애플리케이션 추가를 통해 이 액티비티를 열었을때의 아이템 클릭 로직. 즐겨찾기 앱 DB에 클릭된 앱을 넣는다.
                        FavoriteDBHelper favoritedb;
                        favoritedb = new FavoriteDBHelper(getApplicationContext(), "FavoriteManager", null, 1);
                        if (favoritedb.addApp(info.packageName)) {
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "이미 추가한 애플리케이션입니다.", 1000).show();
                        }
                    }
                    if (!BuildConfig.DEBUG) {
                        easyTracker.send(MapBuilder.createEvent("Press Icon", "Icon Press", "AllAppClick", (long) 16).build());
                    }
                }
            });

            return convertView;
        }
    }

    class ViewHolder {
        ImageView appIcon = null;
        ImageView appIconDelete = null;
        TextView appTitle = null;
    }

    //추천앱을 띄우는 로직
    private class RecommandAdapter extends BaseAdapter {
        LayoutInflater inflater;

        private RecommandAdapter() {
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return recommendAppList.size();
        }

        @Override
        public Object getItem(int position) {
            return recommendAppList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.icon_item, parent, false);
            }

            ImageView imageView = (ImageView) convertView.findViewById(R.id.dock_item_icon_image);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dpToPx(55), dpToPx(55));
            imageView.setLayoutParams(layoutParams);

            TextView textView = (TextView) convertView.findViewById(R.id.dock_item_icon_text);

            imageView.setImageDrawable(recommendAppList.get(position).getIcon());
            if(recommendAppList.get(position).isInstallState() == false) {
                imageView.setAlpha(0.5f);
            }
            textView.setText(recommendAppList.get(position).getTitle());
            String strColor = "#ffffff";
            textView.setTextColor(Color.parseColor(strColor));

            return convertView;
        }
    }

    //유저에게 추천해준 앱 - 지하철, 서울버스, 카카오톡의 리스트를 받아오는 부분.
    public List<RecommendAppModel> getRecommendAppList() {

        int kakaoState, seoulbusState, subwayState;

        SharedPreferences pref = getSharedPreferences("RecommandApps", MODE_PRIVATE);
        kakaoState = pref.getInt("Kakaotalk", 0);
        seoulbusState = pref.getInt("drugalarm", 0);
        subwayState = pref.getInt("Subway", 0);

        List<RecommendAppModel> recommendList = new ArrayList<RecommendAppModel>();
        recommendList.add(new RecommendAppModel("카카오톡", getResources().getDrawable(R.drawable.kakao), "카카오톡입니다.", "https://play.google.com/store/apps/details?id=com.kakao.talk", "com.kakao.talk",(kakaoState==0?false:true)));
        recommendList.add(new RecommendAppModel("나도 약알람", getResources().getDrawable(R.drawable.drugalarm), "나도 약알람앱입니다.", "https://play.google.com/store/apps/details?id=com.nova.medicinealram.app", "com.nova.medicinealram.app",(seoulbusState==0?false:true)));
        recommendList.add(new RecommendAppModel("지하철", getResources().getDrawable(R.drawable.subway), "지하철앱입니다.", "https://play.google.com/store/apps/details?id=com.whitecrow.metroid", "com.whitecrow.metroid",(subwayState==0?false:true)));

        return recommendList;
    }

    //추천앱을 눌렀을때 뜨는 다이얼로그를 정의하기위한 Inner Class
    public static class InstructionDialogFragment extends DialogFragment {

        public static InstructionDialogFragment newInstance(RecommendAppModel model) {

            InstructionDialogFragment frag = new InstructionDialogFragment();
            Bundle args = new Bundle();
            args.putString("appTitle", model.getTitle());
            args.putString("playstoreuri", model.getMarketUrl());
            frag.setArguments(args);

            return frag;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            builder.setMessage(getArguments().getString("appTitle") + "앱을 설치 하시겠어요?")
                    .setTitle(R.string.download)
                    .setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse(getArguments().getString("playstoreuri")));
                            startActivity(intent);
                            if (!BuildConfig.DEBUG) {

                                easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Recommend True", Long.valueOf(8)).build());

                            }
                        }
                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (!BuildConfig.DEBUG) {

                        easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Recommend False", Long.valueOf(8)).build());

                    }
                }
            });

            return builder.create();
        }
    }

    //패키지 명에따라 앱이 설치되어있는지 아닌지를 판별하는 메소드
    public boolean appInstalledOrNot(String uri){
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }catch (PackageManager.NameNotFoundException e){
            app_installed = false;
        }
        return app_installed;
    }

    //Shared Preference를 통하여 유저에게 추천해줄 앱을 세팅하는 함수.
    //Shared Prefence는 GuidActivity 192번째 줄에서부터 관련한 정보를 넣기시작함.
    //앱이 설치되어있었는지 아닌지는 appInstalledOrNot() 메소드에 로직이 마련되어있음.
    //이것을 통해 유저가 추천된 앱을 설치하면 Shared Preference에 설치가 되었다는 사실을 저장한다.
    public void setRecommandAppList(){
        SharedPreferences pref = getSharedPreferences("RecommandApps", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        if (appInstalledOrNot("com.kakao.talk")){
            editor.putInt("Kakaotalk", 1);
        }else{
            editor.putInt("Kakaotalk", 0);
        }

        if (appInstalledOrNot("com.nova.medicinealram.app")){
            editor.putInt("drugalarm", 1);
        }else{
            editor.putInt("drugalarm", 0);
        }

        if (appInstalledOrNot("com.whitecrow.metroid")){
            editor.putInt("Subway", 1);
        }else{
            editor.putInt("Subway", 0);
        }

        editor.commit();
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}
