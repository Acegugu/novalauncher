package com.nova.startlauncher.app.CardClass.Setting;

import android.app.Dialog;
import android.content.Context;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.nova.startlauncher.app.R;

/**
 * Created by FlaShilver on 2014. 4. 4..
 */
public class BrightDialog extends Dialog implements SeekBar.OnSeekBarChangeListener {

    SeekBar brightSeekbar;
    int brightnessVal;
    WindowManager.LayoutParams params;

    public BrightDialog(Context context) {
        super(context);

        init();

    }

    public void init() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_setting_card_brightness);

        params = getWindow().getAttributes();

        try {
            brightnessVal = Settings.System.getInt(getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        brightSeekbar = (SeekBar) findViewById(R.id.setting_card_seekbar);
        brightSeekbar.setOnSeekBarChangeListener(this);
        brightSeekbar.setProgress(brightnessVal);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        brightnessVal = progress;

        if (brightnessVal < 2) {
            brightnessVal = 2;
            brightSeekbar.setProgress(brightnessVal);
        }

        params.screenBrightness = (float) brightnessVal;
        getWindow().setAttributes(params);

        Settings.System.putInt(getContext().getContentResolver(), "screen_brightness", brightnessVal);

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {


    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
