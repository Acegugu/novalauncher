package com.nova.startlauncher.app.Favorite;

import android.graphics.drawable.Drawable;

/**
 * Created by FlaShilver on 2014. 5. 7..
 */
public class FavoriteAppModel {

    String packageName;
    String title;
    Drawable icon;

    public FavoriteAppModel() {
    }

    public FavoriteAppModel(String packageName) {
        this.packageName = packageName;
    }

    public FavoriteAppModel(String packageName, String title, Drawable icon) {
        this.packageName = packageName;
        this.title = title;
        this.icon = icon;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
