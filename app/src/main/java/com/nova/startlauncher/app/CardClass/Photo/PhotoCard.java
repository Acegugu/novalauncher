package com.nova.startlauncher.app.CardClass.Photo;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;
import com.nova.startlauncher.app.Util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by kmkyoung on 2014. 5. 14..
 */
public class PhotoCard extends Card implements View.OnClickListener {
    private String dateFont = "nanum.ttf";
    private ViewFlipper viewFlipper;
    private TextView photoDate;
    private List<PhotoItem> imageList = new ArrayList<PhotoItem>();
    private int width = 0;
    private Boolean updatable = false;
    private PhotoCardObserver photoCardObserver;
    private PhotoUpdateHandler photoUpdateHandler = new PhotoUpdateHandler();
    private ImageView photoSetting;
    public final static int PhotoUpdate = 1111;
    public static Boolean PhotoUpdateFlag = false;

    public PhotoCard(int layout, String cardName, final Context context, int cardId) {
        super(layout, cardName, context, cardId);
        init();
        setListener();
    }

    public void init()
    {
        viewFlipper = (ViewFlipper) cardView.findViewById(R.id.photo_viewFlipper);
        photoSetting = (ImageView)cardView.findViewById(R.id.photo_setting);
        photoDate = (TextView)cardView.findViewById(R.id.photo_date);

        photoDate.setTypeface(Typeface.createFromAsset(context.getAssets(), dateFont));
        photoCardObserver = new PhotoCardObserver(photoUpdateHandler,context);

        viewFlipper.setInAnimation(AnimationUtils.loadAnimation(context,
                R.anim.fadein));
        viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(context,
                R.anim.fadeout));

        viewFlipper.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                width = viewFlipper.getWidth();
                SharedPreferences sharedPreferences = context.getSharedPreferences("photo",Context.MODE_PRIVATE);
                Boolean isSelectPhoto = sharedPreferences.getBoolean("PhotoSelect",false);
                if(!isSelectPhoto) {
                    ImageAsyncTask getImageName = new ImageAsyncTask();
                    getImageName.execute();
                    context.getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, photoCardObserver);
                }
                else
                {
                    PhotoUpdateFlag = true;
                    photoUpdate();
                }
                viewFlipper.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    public void setListener()
    {
        viewFlipper.setOnClickListener(this);
        photoSetting.setOnClickListener(this);
    }

    class PhotoUpdateHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (updatable) {
                ImageAsyncTask getImageName = new ImageAsyncTask();
                getImageName.execute();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.photo_viewFlipper:
                EasyTracker easyTracker = EasyTracker.getInstance(context);
                if (!BuildConfig.DEBUG) {
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "PhotoClick", Long.valueOf(4)).build());
                }
                Intent galleryIntent = new Intent(Intent.ACTION_VIEW, null);
                galleryIntent.setType("image/*");
                galleryIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    context.startActivity(galleryIntent);
                }catch (ActivityNotFoundException e){
                    Toast.makeText(context,R.string.exception_string,1000).show();
                }
                break;
            case R.id.photo_setting:
                Intent photoSelectIntent = new Intent(context,PhotoSelectActivity.class);
                photoSelectIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(photoSelectIntent);
                break;
        }

    }

    public void getImageList() {
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.LATITUDE,
                MediaStore.Images.ImageColumns.LONGITUDE
        };
        final Cursor cursor = context.getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        int imageCount = 0;
        if (cursor.moveToFirst()) {
            while (true) {
                String location = "";
                if(cursor.getString(1) != null) {
                    if (cursor.getString(1).equals("Camera")) {
                        if (cursor.getString(3) != null && cursor.getString(4) != null)
                            location = Utility.findAddress(context, Double.parseDouble(cursor.getString(3)), Double.parseDouble(cursor.getString(4)));

                        PhotoItem photoItem = new PhotoItem(cursor.getString(0), cursor.getString(2), location);
                        imageList.add(photoItem);
                        imageCount++;
                        if (imageCount == 5)
                            break;
                    }
                }
                else
                {
                    try {
                        if (cursor.getString(3) != null && cursor.getString(4) != null)
                            location = Utility.findAddress(context, Double.parseDouble(cursor.getString(3)), Double.parseDouble(cursor.getString(4)));

                        PhotoItem photoItem = new PhotoItem(cursor.getString(0), cursor.getString(2), location);
                        imageList.add(photoItem);
                        imageCount++;
                        if (imageCount == 5)
                            break;
                    }catch (NullPointerException e)
                    { }
                }

                if (!cursor.isLast())
                    cursor.moveToNext();
                else
                    break;
            }
        }
        cursor.close();
    }

    public void bitmapScale() throws IOException {
        Bitmap tempBitmap;

        for (int i = 0; i < imageList.size(); i++) {
            int orientation_val = 0;
            int srcWidth = 0;
            float widthScale;
            try {
                ExifInterface exifInterface = new ExifInterface(imageList.get(i).getName());
                int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                if (orientation == 6)
                    orientation_val = 90;
                else if (orientation == 3)
                    orientation_val = 180;
                else if (orientation == 8)
                    orientation_val = 270;

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageList.get(i).getName(), options);

                if (orientation_val == 90 || orientation_val == 270) {
                    srcWidth = options.outHeight;
                } else {
                    srcWidth = options.outWidth;
                }

                widthScale = srcWidth / width;
                options.inJustDecodeBounds = false;
                options.inSampleSize = (int) widthScale;
                tempBitmap = BitmapFactory.decodeFile(imageList.get(i).getName(), options);

                try {
                    if (tempBitmap != null) {
                        if (orientation_val > 0) {
                            Matrix matrix = new Matrix();
                            matrix.postRotate(orientation_val);
                            tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0, width, tempBitmap.getHeight(), matrix, true);
                        } else {
                            if(width> tempBitmap.getWidth())
                                tempBitmap = Bitmap.createScaledBitmap(tempBitmap,width, (int)(tempBitmap.getHeight() * (width/(double)tempBitmap.getWidth())),true);
                            else
                                tempBitmap = Bitmap.createBitmap(tempBitmap, 0, 0, tempBitmap.getWidth(), tempBitmap.getHeight());
                        }
                        if (tempBitmap != null) {
                            ImageView imageView = new ImageView(context);
                            imageView.setScaleType(ImageView.ScaleType.CENTER);
                            imageView.setImageBitmap(tempBitmap);
                            imageList.get(i).setImageView(imageView);
                        }
                    }
                }catch(OutOfMemoryError e){}
            }
            catch(NullPointerException e)
            {
                Toast.makeText(context, R.string.exception_null_image, 2000).show();
            }
        }
    }



    public void onDestroy() {
        context.getContentResolver().unregisterContentObserver(photoCardObserver);
    }

    class ImageAsyncTask extends AsyncTask<Void, String, Void> {
        @Override
        protected void onPreExecute() {
            if(PhotoUpdateFlag)
            {
                PhotoUpdateFlag = false;
            }
            else {
                updatable = false;
                imageList.clear();
                getImageList();
            }
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            try {
                bitmapScale();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return (null);
        }

        @Override
        protected void onPostExecute(Void unused) {
            viewFlipper.removeAllViews();
            if (imageList.size() != 0) {
                for (int i = 0; i < imageList.size(); i++)
                    viewFlipper.addView(imageList.get(i).getImageview());
            } else {
                //이미지 없을 때 띄울 이미지 앞으로 추가 해야 할 부분
                ImageView imageView = new ImageView(context);
                imageView.setImageResource(R.drawable.picture_default);
                viewFlipper.addView(imageView);
            }

            if (imageList.size() < 2)
            {
                viewFlipper.stopFlipping();
                photoDate.setText("");
            }
            else
            {
                viewFlipper.getInAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        if (imageList.size() > 1) {
                            int currentViewIndex = viewFlipper.getDisplayedChild();
                            if (imageList.get(currentViewIndex).getLocation().equals(""))
                                photoDate.setText(imageList.get(currentViewIndex).getDate());
                            else
                                photoDate.setText(imageList.get(currentViewIndex).getDate() + " - " + imageList.get(currentViewIndex).getLocation() + "에서..");
                        }
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                viewFlipper.startFlipping();
            }

            updatable = true;
        }
    }

    public void photoUpdate()
    {
        if(PhotoUpdateFlag)
        {
            context.getContentResolver().unregisterContentObserver(photoCardObserver);
            loadPhotoItemList();
            ImageAsyncTask getImageName = new ImageAsyncTask();
            getImageName.execute();
        }

    }

    private void loadPhotoItemList()
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("photo",Context.MODE_PRIVATE);
        Set<String> strings = sharedPreferences.getStringSet("photoItemList",null);

        imageList.clear();

        for(String s : strings){
            try{
                JSONObject jsonObject = new JSONObject(s);
                PhotoItem photoItem = new PhotoItem(jsonObject.getString("name"),jsonObject.getString("date"),jsonObject.getString("location"));
                imageList.add(photoItem);

            }catch(JSONException e){
                e.printStackTrace();
            }

        }

    }

}

