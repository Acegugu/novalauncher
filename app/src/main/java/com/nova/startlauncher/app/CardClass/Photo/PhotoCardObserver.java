package com.nova.startlauncher.app.CardClass.Photo;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.util.Log;

import com.nova.startlauncher.app.CardClass.Cost.CostCard;

import javax.security.auth.callback.Callback;

/**
 * Created by kmkyoung on 2014. 5. 18..
 */

public class PhotoCardObserver extends ContentObserver {
    private Context context;
    private Handler handler;
    private String lastPhotoTime = "";

    public PhotoCardObserver(Handler handler, Context context)
    {
        super(handler);
        this.handler = handler;
        this.context = context;
    }

    @Override
    public void onChange(boolean selfChange)
    {
        super.onChange(selfChange);
        String[] projection = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };
        Cursor cursor = context.getContentResolver()
                .query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null,
                        null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                if(!lastPhotoTime.equals(cursor.getString(3))) {
                    if (cursor.getString(2).equals("Camera"))
                        lastPhotoTime = cursor.getString(3);
                        handler.sendMessage(Message.obtain(null, PhotoCard.PhotoUpdate));
                }
            }
        }
        cursor.close();
    }
}

