package com.nova.startlauncher.app.CardClass.Setting;

import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.ToggleButton;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardLauncherActivity;
import com.nova.startlauncher.app.R;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SettingActivity extends Activity implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {
    public static final int backgroundRequestCode = 1111;
    SeekBar brightnessSeekbar;
    int brightnessVal;
    WindowManager.LayoutParams params;

    CheckBox autobrightnessCheckBox;

    SeekBar ringtoneSeekbar;

    int ringthoneVolumeVal;
    private AudioManager audioManager = null;

    ConnectivityManager connectivityManager ;
    Class connectivityManagerClass;
    Method setMobileDataEnabled, getMobileDataEnabled;

    ToggleButton autoscreenrotate;

    ToggleButton dataconnectSwitch;

    ToggleButton wifiSwitch;
    WifiManager wifi;

    ToggleButton gpsSwitch;
    LocationManager locationManager;

    ToggleButton bluetoothSwitch;
    BluetoothAdapter bluetoothAdapter;

    Button launcherButton;
    Button backgroundButton;
    private EasyTracker easyTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        getActionBar().setDisplayHomeAsUpEnabled(true);

        setLayout();
        setListener();

        easyTracker = EasyTracker.getInstance(getApplicationContext());
    }

    private void setLayout() {
        brightnessSeekbar = (SeekBar) findViewById(R.id.activity_setting_brightness_seekbar);
        params = getWindow().getAttributes();

        try {
            brightnessVal = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        brightnessSeekbar.setProgress(brightnessVal);
        //밝기 초기화 끝

        autobrightnessCheckBox = (CheckBox) findViewById(R.id.activity_setting_brightness_checkbox);

        try {
            if (Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE) == 1) {

                autobrightnessCheckBox.setChecked(true);
                brightnessSeekbar.setVisibility(View.INVISIBLE);
            } else {
                autobrightnessCheckBox.setChecked(false);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        //자동 밝기 초기화 끝

        ringtoneSeekbar = (SeekBar) findViewById(R.id.activity_setting_ringtone_seekbar);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        ringthoneVolumeVal = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        ringtoneSeekbar.setProgress(ringthoneVolumeVal);
        ringtoneSeekbar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_RING));
        //벨소리 초기화 끝

        autoscreenrotate = (ToggleButton) findViewById(R.id.activity_setting_autorotate_screen);
        try {
            if(Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION) == 1){
                autoscreenrotate.setChecked(true);
            }else{
                autoscreenrotate.setChecked(false);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        //자동화면회전 초기화 끝

        dataconnectSwitch = (ToggleButton) findViewById(R.id.activity_setting_dataconnect_switch);
        connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(isMobileDataEnable())
            dataconnectSwitch.setChecked(true);
        else
            dataconnectSwitch.setChecked(false);

        wifiSwitch = (ToggleButton) findViewById(R.id.activity_setting_wifi_switch);
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!wifi.isWifiEnabled()) {
            wifiSwitch.setChecked(false);
        } else {
            wifiSwitch.setChecked(true);
        }
        //Wifi 초기화 끝

        gpsSwitch = (ToggleButton) findViewById(R.id.activity_setting_gps_switch);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            gpsSwitch.setChecked(false);
        } else {
            gpsSwitch.setChecked(true);
        }
        //GPS 초기화 끝

        bluetoothSwitch = (ToggleButton) findViewById(R.id.activity_setting_bluetooth_switch);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
            bluetoothSwitch.setChecked(true);
        } else {
            bluetoothSwitch.setChecked(false);
        }
        //블루투스 초기화 끝

        launcherButton = (Button) findViewById(R.id.activity_setting_launcher_button);
        backgroundButton = (Button)findViewById(R.id.activity_setting_background_button);
    }

    private void setListener() {
        //각종 버튼의 액션에 알맞는 implements된 리스너를 this키워드를 통해서 각 버튼요소에 등록시킴.
        brightnessSeekbar.setOnSeekBarChangeListener(this);
        ringtoneSeekbar.setOnSeekBarChangeListener(this);
        autobrightnessCheckBox.setOnCheckedChangeListener(this);
        dataconnectSwitch.setOnCheckedChangeListener(this);
        wifiSwitch.setOnCheckedChangeListener(this);
        gpsSwitch.setOnCheckedChangeListener(this);
        bluetoothSwitch.setOnCheckedChangeListener(this);
        autoscreenrotate.setOnCheckedChangeListener(this);
        launcherButton.setOnClickListener(this);
        backgroundButton.setOnClickListener(this);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        switch (seekBar.getId()) {
            //유저가 밝기 조정 SeekBar를 움직일 때
            case R.id.activity_setting_brightness_seekbar:

                brightnessVal = progress;

                if (brightnessVal < 2) {
                    brightnessVal = 2;
                    brightnessSeekbar.setProgress(brightnessVal);
                }

                params.screenBrightness = (float) brightnessVal;
                getWindow().setAttributes(params);

                Settings.System.putInt(getContentResolver(), "screen_brightness", brightnessVal);
                break;

            //유저가 벨소리 조정 SeekBar를 움직일 때
            case R.id.activity_setting_ringtone_seekbar:

                ringthoneVolumeVal = progress;

                audioManager.setStreamVolume(AudioManager.STREAM_RING, ringthoneVolumeVal, 0);

                break;
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //유저가 SeekBar에 손을 올려놓을 때 약한 진동이 울리게 한다.
        Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(200);

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        switch (buttonView.getId()) {
            //유저가 자동 밝기 Switch를 움직일 때
            case R.id.activity_setting_brightness_checkbox:
                //자동 밝기를 설정하면 기존 밝기조정 SeekBar는 사라지고, 자동 밝기를 해제하면 기존 밝기조정 SeekBar가 나타난다.
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, isChecked ? Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC : Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

                brightnessSeekbar.setVisibility(isChecked ? View.INVISIBLE : View.VISIBLE);

                break;

            case R.id.activity_setting_autorotate_screen:
                //유저가 자동화면회전 Switch를 움직일 때
                Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, isChecked ? 1 : 0);

                break;

            case R.id.activity_setting_dataconnect_switch:
                if(isMobileDataEnable())
                    toggleMobileData(false);
                else
                    toggleMobileData(true);
                //유저가 데이타 사용 Switch를 움직일 때
                break;

            case R.id.activity_setting_wifi_switch:
                //유저가 Wifi 사용 Switch를 움직일 때
                wifi.setWifiEnabled(isChecked ? true : false);

                break;

            case R.id.activity_setting_gps_switch:
                //유저가 GPS 사용 Switch를 움직일 때

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    //Android KitKat 이상 혹은 2.3이상부터는 GPS를 개발자가 마음대로 On/Off 할 수 없음.
                    //이것을 가능하게 하는방법은 Google이 설정한 고유 GPS 셋업 설정 액티비티로 이동하여야한다.
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    //위에서 걸은 조건 이하의 안드로이드 버전에서는 GPS가 켜지고 꺼지게했음.
                    if (isChecked == true) {
                        turnGPSOn();
                    } else {
                        turnGPSOff();
                    }
                    gpsSwitch.setChecked(isChecked ? true : false);
                }

                break;

            case R.id.activity_setting_bluetooth_switch:
                //유저가 블루투스 사용 Switch를 움직일 때

                if (isChecked == true) {
                    bluetoothAdapter.enable();

                } else {
                    bluetoothAdapter.disable();
                }

                break;
            default:
                break;
        }

    }

    public boolean isMobileDataEnable()
    {
        try {
            connectivityManagerClass = Class.forName(connectivityManager.getClass().getName());
            getMobileDataEnabled = connectivityManagerClass.getDeclaredMethod("getMobileDataEnabled");
            getMobileDataEnabled.setAccessible(true);
            dataconnectSwitch.setChecked((Boolean)getMobileDataEnabled.invoke(connectivityManager));
            return (Boolean)getMobileDataEnabled.invoke(connectivityManager);
        } catch (Exception e) {
            return false;
        }
    }

    public void toggleMobileData(boolean enable)
    {
        try {
            setMobileDataEnabled = connectivityManagerClass.getDeclaredMethod("setMobileDataEnabled",Boolean.TYPE);
            setMobileDataEnabled.setAccessible(true);
            setMobileDataEnabled.invoke(connectivityManager, enable);
            dataconnectSwitch.setChecked(enable);
        } catch (Exception e){
        }
    }

    public void turnGPSOn() {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        sendBroadcast(intent);

    }

    public void turnGPSOff() {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        sendBroadcast(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();

        setLayout();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!BuildConfig.DEBUG){
            EasyTracker.getInstance(this).activityStart(this);
        }

        setLayout();
        setListener();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (!BuildConfig.DEBUG){
            EasyTracker.getInstance(this).activityStop(this);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_setting_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_setting_launcher_button:
                // 기본 런처 선택
                ComponentName componentName = new ComponentName(this, FakeHome.class);
                if (!isMyLauncherDefault()) {
                    // toggle fake activity
                    PackageManager pm = getPackageManager();
                    int flag = ((pm.getComponentEnabledSetting(componentName) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) ? PackageManager.COMPONENT_ENABLED_STATE_DISABLED
                            : PackageManager.COMPONENT_ENABLED_STATE_ENABLED);
                    pm.setComponentEnabledSetting(componentName, flag, PackageManager.DONT_KILL_APP);

                } else {
                    resetPreferedLauncher();
                }
                // start home activity to enable chooser
                Intent selector = new Intent(Intent.ACTION_MAIN);
                selector.addCategory(Intent.CATEGORY_HOME);
                startActivity(selector);

                break;
            case R.id.activity_setting_background_button:
                Intent intent = new Intent(this,BackgroundActivity.class);
                startActivity(intent);
                if (!BuildConfig.DEBUG){
                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Setting Activity Background", Long.valueOf(6)).build());
                }
                break;
            default:
                break;
        }
    }

    private boolean isMyLauncherDefault() {
        final IntentFilter filter = new IntentFilter(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);

        List<IntentFilter> filters = new ArrayList<IntentFilter>();
        filters.add(filter);

        final String myPackageName = getPackageName();
        List<ComponentName> activities = new ArrayList<ComponentName>();
        final PackageManager packageManager = getPackageManager();

        // You can use name of your package here as third argument
        packageManager.getPreferredActivities(filters, activities, null);

        for (ComponentName activity : activities) {
            if (myPackageName.equals(activity.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    // 기본 런처 설정을 Clear
    private void resetPreferedLauncher() {
        PackageManager pm = getPackageManager();

        // 다른 앱의 clear는 SecurityException이라 자신만 default 설정을 해제할 수 있다.
        pm.clearPackagePreferredActivities(this.getPackageName());
    }
}
