package com.nova.startlauncher.app.Util;

import android.graphics.drawable.Drawable;

/**
 * Created by FlaShilver on 2014. 6. 2..
 */
public class RecommendAppModel {

    private String title;
    private Drawable icon;
    private String instruction;
    private String marketUrl;
    private boolean installState;
    private String packageName;

    public RecommendAppModel(String title, Drawable icon, String instruction, String marketUrl, String packagename,boolean installState) {
        this.title = title;
        this.icon = icon;
        this.instruction = instruction;
        this.marketUrl = marketUrl;
        this.installState = installState;
        this.packageName = packagename;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getMarketUrl() {
        return marketUrl;
    }

    public void setMarketUrl(String marketUrl) {
        this.marketUrl = marketUrl;
    }

    public String getPackageName() { return packageName; }

    public boolean isInstallState() {
        return installState;
    }

    public void setInstallState(boolean installState) {
        this.installState = installState;
    }
}
