package com.nova.startlauncher.app.CardClass.Cost;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.CallLog;

/**
 * Created by kmkyoung on 2014. 4. 14..
 */
public class CostCardCallObserver extends ContentObserver {
    private final String TAG = "CostCardCallObserver";
    private Context context;
    private final Uri CALL_URI = android.provider.CallLog.Calls.CONTENT_URI;
    private Messenger sendMessenger;
    private String preDate ="";

    public CostCardCallObserver(Handler handler, Context context, Messenger messenger) {
        super(handler);
        this.context = context;
        sendMessenger = messenger;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        Cursor callCursor = context.getContentResolver().query(CALL_URI, null, null, null, null);
        if (callCursor != null) {
            if (callCursor.moveToLast()) {
                Long duration = callCursor.getLong(callCursor.getColumnIndex(CallLog.Calls.DURATION));
                String date = callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.DATE));
                String new_number = callCursor.getString(callCursor.getColumnIndex(CallLog.Calls.NEW));

                if(new_number.equals("1") && !date.equals(preDate))
                {
                    SharedPreferences sharedpreferences = context.getSharedPreferences("Cost", Context.MODE_MULTI_PROCESS);
                    SharedPreferences.Editor editor = sharedpreferences.edit();

                    int call = sharedpreferences.getInt("CostUsingCall", 0) + duration.intValue();

                    editor.putInt("CostUsingCall", call);
                    editor.commit();

                    try {
                        if (sendMessenger != null)
                            sendMessenger.send(Message.obtain(null, CostCard.SEND_CALL_UPDATE));
                    } catch (RemoteException e) {

                    }
                    preDate = date;
                }

            }

        }
    }
}
