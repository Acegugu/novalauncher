package com.nova.startlauncher.app.CardClass.Watch;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.CardClass.AllApp.AllAppActivity;
import com.nova.startlauncher.app.CardClass.AllApp.AllAppCard;
import com.nova.startlauncher.app.CardClass.Card;
import com.nova.startlauncher.app.R;

/**
 * Created by kmkyoung on 2014. 5. 3..
 */

public class WatchCard extends Card implements View.OnClickListener{
    private static final String dateFont = "SeoulNamsanB.otf";
    private static final String timeFont = "SeoulNamsanB.otf";
    private static final String ampmFont = "SeoulNamsanB.otf";
    private static final String mottoFont = "SeoulNamsanB.otf";
    public static final int mottoRequestCode = 114;

    TextView mottoTextView;
    ImageView appAppIcon;
    ImageView wifiIcon;
    Activity activity;
    ProgressBar wifiProgressBar;

    private WifiManager wifimanager = (WifiManager)context.getSystemService(context.WIFI_SERVICE);

    WatchDigitalClock dateTextView;
    WatchDigitalClock timeTextView;
    WatchDigitalClock ampmTextView;
    private EasyTracker easyTracker;

    public WatchCard(int layout, String cardName, Context context, int cardId) {
        super(layout, cardName, context, cardId);
        setLayout();
        setListener();
        init();
    }

    public void init()
    {
        easyTracker = EasyTracker.getInstance(context);

        SharedPreferences sharedpreferences = context.getSharedPreferences("Watch", context.MODE_MULTI_PROCESS);
        String motto = sharedpreferences.getString("WatchMotto","");
        if(motto == "")
            mottoTextView.setText(R.string.watch_motto_default);
        else
            mottoTextView.setText(motto);

        IntentFilter intentfilter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
        context.registerReceiver(WatchCardReceiver, intentfilter);
    }

    public void setLayout()
    {
        appAppIcon = (ImageView)cardView.findViewById(R.id.watch_allAppIcon);
        wifiIcon = (ImageView)cardView.findViewById(R.id.watch_wifiIcon);
        wifiProgressBar = (ProgressBar)cardView.findViewById(R.id.watch_card_wifi_progressbar);
        mottoTextView = (TextView)cardView.findViewById(R.id.watchphoto_card_motto);
        mottoTextView.setTypeface(Typeface.createFromAsset(context.getAssets(), mottoFont));
        dateTextView = (WatchDigitalClock)cardView.findViewById(R.id.watchphoto_card_date);
        timeTextView = (WatchDigitalClock)cardView.findViewById(R.id.watchphoto_card_time);
        ampmTextView = (WatchDigitalClock)cardView.findViewById(R.id.watchphoto_card_ampm);
        dateTextView.setFormat(0);
        timeTextView.setFormat(1);
        ampmTextView.setFormat(2);
        dateTextView.setTypeface(Typeface.createFromAsset(context.getAssets(),dateFont));
        timeTextView.setTypeface(Typeface.createFromAsset(context.getAssets(),timeFont));
        ampmTextView.setTypeface(Typeface.createFromAsset(context.getAssets(),ampmFont));
        mottoTextView.setSelected(true);

    }

    public void setListener()
    {
        mottoTextView.setOnClickListener(this);
        dateTextView.setOnClickListener(this);
        timeTextView.setOnClickListener(this);
        ampmTextView.setOnClickListener(this);
        appAppIcon.setOnClickListener(this);
        wifiIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId())
        {
            case R.id.watchphoto_card_motto:
                intent = new Intent(activity, WatchMottoActivity.class);
                activity.startActivityForResult(intent, mottoRequestCode);
                activity.overridePendingTransition(R.anim.fade_in_motto, R.anim.fade_out_motto);
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Motto Activity", Long.valueOf(8)).build());

                }
                break;

            case R.id.watch_allAppIcon:
                intent = new Intent(activity, AllAppActivity.class);
                intent.putExtra("callStage", AllAppCard.CALL_FROM_ALL_APP_CARD);
                activity.startActivity(intent);
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Watch Side AllApp", Long.valueOf(8)).build());

                }
                break;

            case R.id.watch_wifiIcon:
                wifimanager.setWifiEnabled(!wifimanager.isWifiEnabled());
                if(!wifimanager.isWifiEnabled())
                {
                    wifiProgressBar.setVisibility(View.VISIBLE);
                    wifiIcon.setVisibility(View.INVISIBLE);
                    Handler mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            wifiProgressBar.setVisibility(View.INVISIBLE);
                            wifiIcon.setVisibility(View.VISIBLE);
                        }
                    }, 2000);
                }
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Watch Side Wifi", Long.valueOf(8)).build());

                }
                break;

            default:
                intent = new Intent(AlarmClock.ACTION_SHOW_ALARMS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    context.startActivity(intent);
                }catch(ActivityNotFoundException e){
                    Toast.makeText(context,R.string.exception_string,1000).show();
                }
                if (!BuildConfig.DEBUG) {

                    easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Watch Alarm", Long.valueOf(8)).build());

                }
        }
    }

    public void setActivity(Activity activity)
    {
        this.activity = activity;
    }

    public void onActivityResult(int requestCode,int resultCode, Intent data)
    {
        if(resultCode == -1)
        {
            if(requestCode == mottoRequestCode)
            {
                String motto = data.getStringExtra("watch_motto");
                SharedPreferences sharedpreferences = context.getSharedPreferences("Watch",context.MODE_MULTI_PROCESS);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("WatchMotto",motto);
                editor.commit();
                if(motto.equals(""))
                    mottoTextView.setText(R.string.watch_motto_default);
                else {
                    mottoTextView.setText(data.getStringExtra("watch_motto"));
                    if (!BuildConfig.DEBUG) {

                        easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "Watch Motto Done", Long.valueOf(8)).build());

                    }
                }
            }
        }
    }

    private BroadcastReceiver WatchCardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(wifimanager.isWifiEnabled()) {
                wifiIcon.setImageResource(R.drawable.top_wifi_on_selector);
            }
            else {
                wifiIcon.setImageResource(R.drawable.top_wifi_off_selector);
            }
        }
    };
}
