package com.nova.startlauncher.app.CardClass.NewContactBook;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.nova.startlauncher.app.BuildConfig;
import com.nova.startlauncher.app.R;

import java.util.ArrayList;

/**
 * Contact Fragment for ViewPager
 */
public class ContactFragment extends Fragment {

    private static final String PAGE_INDEX_KEY = "pageIndex";
    private static final String PAGE_ITEMS_KEY = "items";
    private final static String contactNameFont = "SeoulNamsan.ttf";
    private final static String contactInfoNameFont = "SeoulNamsanB.otf";
    private final static String contactInfoBtnFont = "SeoulNamsan.ttf";
    private String pageIndex;
    private ArrayList<Contact> pageContacts;

    EasyTracker easyTracker;

    static ContactFragment newInstance(String pageNumber, ArrayList<Contact> contactsWithPageIndex) {
        final ContactFragment fragment = new ContactFragment();

        final Bundle args = new Bundle();
        args.putString(PAGE_INDEX_KEY, pageNumber);
        args.putParcelableArrayList(PAGE_ITEMS_KEY, contactsWithPageIndex);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageIndex = getArguments() != null ? getArguments().getString(PAGE_INDEX_KEY) : "";
        pageContacts = getArguments() != null ? getArguments().<Contact>getParcelableArrayList(PAGE_ITEMS_KEY) : null;
        easyTracker = EasyTracker.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.contact_fragment_page, container, false);
        final ViewGroup nameListView = (ViewGroup) rootView.findViewById(R.id.contact_name_page);
        final ViewGroup contactInfoView = (ViewGroup) rootView.findViewById(R.id.contact_info);
        final TextView contactIndex = (TextView) rootView.findViewById(R.id.contact_index);
        contactIndex.setSelected(true);

        int padding_in_dp = 6;  // 6 dps
        final float scale = getResources().getDisplayMetrics().density;
        int padding_in_px = (int) (padding_in_dp * scale + 0.5f);

        if (pageContacts.size() != 0) {
            for (final Contact item : pageContacts) {
                TextView newView = new TextView(getActivity());
                newView.setText(item.name);
                newView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactNameFont));
                newView.setTextSize(23);
                newView.setMaxLines(1);
                newView.setEllipsize(TextUtils.TruncateAt.END);
                newView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView nameView = (TextView) contactInfoView.findViewById(R.id.contact_info_name);
                        Button callButton = (Button) contactInfoView.findViewById(R.id.contact_info_call);
                        Button messageButton = (Button) contactInfoView.findViewById(R.id.contact_info_message);
                        Button backButton = (Button) contactInfoView.findViewById(R.id.contact_info_back);

                        nameView.setText(item.name);
                        nameView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactInfoNameFont));
                        callButton.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactInfoBtnFont));
                        messageButton.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactInfoBtnFont));
                        backButton.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactInfoBtnFont));
                        callButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + item.phone_number));
                                getActivity().startActivity(intent);
                            }
                        });
                        messageButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", item.phone_number, null));
                                getActivity().startActivity(intent);
                            }
                        });
                        backButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                nameListView.setVisibility(View.VISIBLE);
                                contactInfoView.setVisibility(View.GONE);
                            }
                        });
                        nameListView.setVisibility(View.GONE);
                        contactInfoView.setVisibility(View.VISIBLE);

                        if (!BuildConfig.DEBUG) {

                            easyTracker.send(MapBuilder.createEvent("Press Action", "Button Press", "ShowContact", Long.valueOf(2)).build());

                        }
                    }
                });
                newView.setPadding(0, padding_in_px, 0, 0);
                nameListView.addView(newView);

                View divider = new View(getActivity());
                divider.setBackgroundColor(getResources().getColor(R.color.contact_name_divider));
                divider.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 3));
                nameListView.addView(divider);
            }
        } else {
            TextView emptyDataView = new TextView(getActivity());
            emptyDataView.setText("'" + pageIndex + "' 항목에 해당하는 연락처가 없습니다");
            emptyDataView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), contactNameFont));
            emptyDataView.setTextSize(23);
            emptyDataView.setPadding(0, padding_in_px, 0, 0);
            nameListView.addView(emptyDataView);
        }
        contactIndex.setText(pageIndex);

        final ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.contact_viewpager);
        Button previousButton = (Button) rootView.findViewById(R.id.contact_page_previous);
        Button nextButton = (Button) rootView.findViewById(R.id.contact_page_next);
        if(previousButton != null) {
            previousButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(NewContactBook.VIEWPAGER_ACTION);
                    intent.putExtra("DATA", "PREVIOUS");
                    getActivity().sendBroadcast(intent);
                }
            });
        }
        if (nextButton != null) {
            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(NewContactBook.VIEWPAGER_ACTION);
                    intent.putExtra("DATA", "NEXT");
                    getActivity().sendBroadcast(intent);
                }
            });
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
