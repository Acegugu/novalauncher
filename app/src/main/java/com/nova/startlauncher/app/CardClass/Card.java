package com.nova.startlauncher.app.CardClass;

import android.content.Context;
import android.os.Build;
import android.view.View;

/**
 * Created by FlaShilver on 2014. 4. 2..
 */
public abstract class Card {
    protected View cardView;
    String cardName;
    int cardId;
    protected Context context;

    public Card(int layout, String cardName, Context context, int cardId) {
        this.cardName = cardName;
        this.context = context;
        this.cardId = cardId;
        this.cardView = View.inflate(context, layout, null);
    }

    public View getCardView() {
        return cardView;
    }

    public void setCardView(View cardView) {
        this.cardView = cardView;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public void setListener(){

    }
}